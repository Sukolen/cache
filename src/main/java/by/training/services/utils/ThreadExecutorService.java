package by.training.services.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Ruslan on 23.01.2018.
 */
public class ThreadExecutorService {
    private final ExecutorService executor;

    /**
     * Constructor
      * @param threadPoolCount thread count
     */
    public ThreadExecutorService(final Integer threadPoolCount) {
        this.executor = Executors.newFixedThreadPool(threadPoolCount);
    }

    /**
     * Getter for executor
     * @return executor
     */
    public ExecutorService getExecutor() {
        return executor;
    }
}
