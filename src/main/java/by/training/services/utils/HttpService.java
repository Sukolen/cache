package by.training.services.utils;

import by.training.constants.Constants;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * HttpService for data exchange between nodes
 */
public class HttpService {
    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private final String http = "http://";
    private final String statusCode = "statusCode";
    private final String contentType = "Content-type";
    private final String trueParam = "true";
    private final Logger logger = LogManager.getLogger();
    private final RequestConfig defaultRequestConfig;

    /**
     * Constructor
     * @param requestTimeOut Timeout request
     */
    public HttpService(final int requestTimeOut) {
        this.defaultRequestConfig = RequestConfig
                .copy(RequestConfig.DEFAULT)
                .setConnectionRequestTimeout(requestTimeOut)
                .build();
    }

    /**
     * Method send HTTP POST request
     * @param server Server address
     * @param reqUrl URL
     * @param body Request body
     * @return Map response (status code and body)
     */
    public Map<String, Object> sendPost(String server, String reqUrl, String body) {
        Map<String, Object> result = new HashMap<>();
        try {
            HttpPost httpPost = new HttpPost(http + server + reqUrl);
            httpPost.setConfig(defaultRequestConfig);
            HttpEntity httpEntity = null;
            try {
                httpEntity = new StringEntity(body);
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage());
            }
            httpPost.setEntity(httpEntity);
            httpPost.setHeader(contentType, Constants.CONTENT_TYPE_JSON);
            httpPost.setHeader(Constants.SERVLET_PARAM_MASTER, trueParam);

//            CloseableHttpResponse response;
//            response = httpClient.execute(httpPost);
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                result.put(statusCode, response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            result.put(statusCode, HttpServletResponse.SC_BAD_REQUEST);
        }
        return result;
    }

    /**
     * Method send HTTP PUT request
     * @param server Server address
     * @param reqUrl URL
     * @param body Request body
     * @return Map response (status code and body)
     */
    public Map<String, Object> sendPut(String server, String reqUrl, String body) {
        Map<String, Object> result = new HashMap<>();
        try {

            HttpPut httpPut = new HttpPut(http + server + reqUrl);
            httpPut.setConfig(defaultRequestConfig);
            HttpEntity httpEntity = null;
            try {
                httpEntity = new StringEntity(body);
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage());
            }
            httpPut.setEntity(httpEntity);
            httpPut.setHeader(contentType, Constants.CONTENT_TYPE_JSON);
            httpPut.setHeader(Constants.SERVLET_PARAM_MASTER, trueParam);
            try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
                result.put(statusCode, response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            result.put(statusCode, HttpServletResponse.SC_BAD_REQUEST);
        }
        return result;
    }

    /**
     * Method send HTTP DELETE request
     * @param server Server address
     * @param reqUrl URL
     * @return Map response (status code)
     */
    public Map<String, Object> sendDelete(String server, String reqUrl) {
        Map<String, Object> result = new HashMap<>();
        try {
            HttpDelete httpDelete = new HttpDelete(http + server + reqUrl);
            httpDelete.setConfig(defaultRequestConfig);
            httpDelete.setHeader(Constants.SERVLET_PARAM_MASTER, trueParam);
            try (CloseableHttpResponse response = httpClient.execute(httpDelete)) {
                result.put(statusCode, response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            result.put(statusCode, HttpServletResponse.SC_BAD_REQUEST);
        }
        return result;
    }

    /**
     * Method send HTTP GET request
     * @param server Server address
     * @param reqUrl URL
     * @return Map response (status code)
     */
    public Map<String, Object> sendGet(String server, String reqUrl) {
        Map<String, Object> result = new HashMap<>();
        try {
            HttpGet httpGet = new HttpGet(http + server + reqUrl);
            httpGet.setConfig(defaultRequestConfig);
            httpGet.setHeader(Constants.SERVLET_PARAM_MASTER, trueParam);
            httpGet.setHeader(contentType, Constants.CONTENT_TYPE_JSON);
            try (CloseableHttpResponse response = httpClient.execute(httpGet);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader br = new BufferedReader(inputStreamReader)) {

                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }
                result.put(statusCode, response.getStatusLine().getStatusCode());
                result.put(Constants.RESPONSE_BODY, stringBuilder.toString());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            result.put(statusCode, HttpServletResponse.SC_BAD_REQUEST);
        }
        return result;
    }
}
