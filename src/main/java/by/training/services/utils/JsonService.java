package by.training.services.utils;

import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Ruslan on 03.12.2017.
 */
public final class JsonService {
    private static final String NAME_FILED = "name";
    private static final String TYPE_CACHE_FIELD = "type";
    private static final String MAX_COUNT_FIELD = "maxCount";
    private static final String JSON_SCHEMA_FIELD = "jsonSchema";
    private static final String VALUE_FIELD = "value";

    private JsonService() {
    }

    /**
     * Parse object collection (ObjectCollection) from JSON
     * @param name name collection
     * @param json JSON
     * @return object collection (ObjectCollection)
     */
    public static ObjectCollection getCollectionFromJSON(final String name, final String json) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(json);
        if ((jo.has(TYPE_CACHE_FIELD)) && (jo.has(MAX_COUNT_FIELD)) && (jo.has(JSON_SCHEMA_FIELD))) {
            String type = jo.get(TYPE_CACHE_FIELD).getAsString();
            int maxCount = jo.get(MAX_COUNT_FIELD).getAsInt();
            String jsonSchema = jo.getAsJsonObject(JSON_SCHEMA_FIELD).toString();

            return new ObjectCollection(name, type, maxCount, jsonSchema);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Parse object collection (ObjectCollection) from JSON without schema
     * @param json JSON
     * @return object collection (ObjectCollection)
     */
    public static ObjectCollection getCollectionFromJSONWithoutSchema(final String json) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(json);
        if ((jo.has(NAME_FILED)) && (jo.has(TYPE_CACHE_FIELD)) && (jo.has(MAX_COUNT_FIELD))) {
            String name = jo.get(NAME_FILED).getAsString();
            String type = jo.get(TYPE_CACHE_FIELD).getAsString();
            int maxCount = jo.get(MAX_COUNT_FIELD).getAsInt();
            return new ObjectCollection(name, type, maxCount);
        } else {
            throw new IllegalArgumentException();
        }

    }

    /**
     * Return object value from json string
     * @param json JSON string
     * @return Object value
     */
    public static ObjectValue getObjectFromJSON(final String json) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(json);
        if ((jo.has(NAME_FILED)) && (jo.has(VALUE_FIELD))) {
            String name = jo.get(NAME_FILED).getAsString();
            String value = jo.get(VALUE_FIELD).getAsString();

            return new ObjectValue(name, value);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Get JSON from object
     * @param value object
     * @param <T> type object
     * @return JSON string
     */
    public static <T> String getJSON(final T value) {
        Gson gson = new Gson();
        return  gson.toJson(value);
    }

    /**
     * Get JSON from list collections without schema
     * @param objectCollections list of objectCollections
     * @return JSON string
     */
    public static String getJSONFromCollections(final List<ObjectCollection> objectCollections) {
        JsonArray jsonArray = getJsonArray(objectCollections);
        for (JsonElement jsonElement: jsonArray) {
            jsonElement.getAsJsonObject().remove(JSON_SCHEMA_FIELD);
        }
        return jsonArray.toString();
    }

    /**
     * Get JSON from list of objectValues
     * @param objectValues list of objectValues
     * @return JSON string
     */
    public static String getJSONFromObjects(final List<ObjectValue> objectValues) {
        JsonArray jsonArray = getJsonArray(objectValues);
        return jsonArray.toString();
    }

    /**
     * Get list objects value from json array
     * @param array JSON array as string
     * @return List objects value
     */
    public static List<ObjectValue>  getObjectsFromJson(String array) {
        Type collectionType = new TypeToken<List<ObjectValue>>() { }.getType();
        return new Gson().fromJson(array, collectionType);
    }

    /**
     * Get JsonArray object from List<T>
     * @param list List objects
     * @param <T> Type objects
     * @return JsonArray object
     */
    public static <T> JsonArray getJsonArray(final List<T> list) {
        return new Gson().toJsonTree(list).getAsJsonArray();
    }

    /**
     * Validate JSON
     * @param jsonSchema schema JSON
     * @param value JSON
     * @return exception if value not valid
     */
    public static boolean validateJSON(final String jsonSchema, final String value) {
        JSONObject rawSchema = new JSONObject(new JSONTokener(jsonSchema));
        Schema schema = SchemaLoader.load(rawSchema);
        schema.validate(new JSONObject(value));
        return true;
    }

    /**
     * Get JsonObject from InputStream
     * @param stream InputStream object
     * @return JsonObject object
     */
    public static JsonObject getJsonObjectFromStream(final InputStream stream) {
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(new InputStreamReader(stream));
        return jsonElement.getAsJsonObject();
    }
}
