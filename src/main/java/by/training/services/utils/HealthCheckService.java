package by.training.services.utils;

import by.training.beans.GroupNode;
import by.training.beans.ServerNode;
import by.training.constants.Constants;
import by.training.exceptions.NodeException;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Service for check life servers
 */
public class HealthCheckService {
    private final HttpService httpService;
    private final List<GroupNode> serversGroup;
    private final ThreadExecutorService executor;
    private final Integer updateTime;
    private final String statusCode = "statusCode";
    private final Logger logger = LogManager.getLogger();

    /**
     * Constructor
     * @param httpService Http service
     * @param serversGroup List all groups server
     * @param executor ExecutorService
     * @param updateTime Update timeout for server status
     */
    public HealthCheckService(final HttpService httpService, final List<GroupNode> serversGroup,
                              final ThreadExecutorService executor, final Integer updateTime) {
        this.httpService = httpService;
        this.serversGroup = serversGroup;
        this.executor = executor;
        this.updateTime = updateTime;
    }

    /**
     * Get list dead servers
     * @return List dead servers
     */
    public List<String> getDeadServers() {
        List<String> result = new ArrayList<>();
        serversGroup.stream()
                .forEach(groupNode -> groupNode.getServerNodes().stream()
                        .filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                            if (LocalDateTime.now().minusSeconds(updateTime).isBefore(item.getDateTime())) {
                                Future<Map<String, Object>> future = executor.getExecutor().submit(
                                        () -> httpService.sendGet(item.getAddress(), "/app/info"));
                                Map<String, Object> resultCode;
                                ReentrantLock lock = new ReentrantLock();
                                try {
                                    resultCode = future.get();
                                } catch (ExecutionException e) {
                                    logger.error(e.getMessage());
                                    resultCode = new HashMap<>();
                                    resultCode.put(statusCode, HttpServletResponse.SC_BAD_REQUEST);
                                } catch (InterruptedException e) {
                                    future.cancel(true);
                                    logger.error(e.getMessage());
                                    resultCode = new HashMap<>();
                                    resultCode.put(statusCode, HttpServletResponse.SC_BAD_REQUEST);
                                }
                                lock.lock();
                                try {
                                    item.setDateTime(LocalDateTime.now());
                                    if ((Integer) resultCode.get(statusCode) != HttpServletResponse.SC_OK) {
                                        item.setHealth(false);
                                    } else {
                                        item.setHealth(true);
                                        String response = (String) resultCode.get(Constants.RESPONSE_BODY);
                                        if (response != null) {
                                            Gson gson = new Gson();
                                            if (gson.fromJson(response, Boolean.class)) {
                                                item.setIsRebalanceCollection(true);
                                            }
                                        }
                                    }
                                } finally {
                                    lock.unlock();
                                }
                                if ((Integer) resultCode.get(statusCode) != HttpServletResponse.SC_OK) {
                                    result.add(item.getAddress());
                                }

                            } else {
                                if (!item.getHealth()) {
                                    result.add(item.getAddress());
                                }
                            }
        })));
        return result;
    }

    /**
     * Check server on life
     * @param server Server
     * @throws NodeException Exception if server dead
     */
    public void checkServer(ServerNode server) throws NodeException {
        if (getDeadServers().stream().anyMatch(s -> s.equals(server.getAddress()))) {
            throw new NodeException("Node '" + server.getAddress() + "' is not responding.");
        }
    }

    /**
     * Check group servers on life
     * @param serverNode List of servers
     * @throws NodeException Exception if  any server dead
     */
    public void checkServersGroup(final List<ServerNode> serverNode) throws NodeException {
        List<String> deadNodes = getDeadServers();
        for (String server: deadNodes) {
            if (serverNode.stream()
                    .filter(item -> !item.getMaster())
                    .anyMatch(item -> item.getAddress().equals(server))) {
                throw new NodeException("Node '" + server + "' is not responding.");
            }
        }
    }

    /**
     * Check all server
     * @throws NodeException NodeException Exception if  any server dead
     */
    public void checkServers() throws NodeException {
        List<String> servers = getDeadServers();
        if (servers.size() > 0) {
            throw new NodeException("Node '" + servers.toString() + "' is not responding");
        }
    }

    /**
     * Check rebalance collection on all servers
     * @return All rebalance or not
     */
    public Boolean isAllRebalanceCollection() {
        Boolean result = false;
        for (GroupNode groupNode: serversGroup) {
            List<ServerNode> serverNodes = groupNode.getServerNodes();
            if (serverNodes.stream()
                    .filter(serverNode1 -> !serverNode1.getMaster())
                    .anyMatch(serverNode -> !serverNode.getIsRebalanceCollection())) {
                result = false;
                break;
            } else {
                result = true;
            }
        }
        return result;
    }

}
