package by.training.services;

import by.training.exceptions.MergeConflictException;
import by.training.exceptions.NotFoundException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import by.training.exceptions.DaoException;
import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Object collection service
 */
public class ObjectService {
    private Map<String, ICache<String, ObjectValue>> cacheMap;
    private IValueDAO valueDAO;
    private ICollectionDAO collectionDAO;


    /**
     * Constructor
     * @param cacheMap map caches
     * @param collectionDAO collection DAO
     * @param valueDAO objectValue DAO
     */
    public ObjectService(final Map<String, ICache<String, ObjectValue>> cacheMap,
                         final ICollectionDAO collectionDAO, final IValueDAO valueDAO) {
        this.cacheMap = cacheMap;
        this.collectionDAO = collectionDAO;
        this.valueDAO = valueDAO;
    }

    /**
     * Create objectValue in cache&DB
     * @param collectionName name collection
     * @param objectValue objectValue
     * @throws DaoException DAO exception
     * @throws NotFoundException if collection not found in DAO
     */
    public void createObject(final String collectionName,
                                           final ObjectValue objectValue) throws DaoException, NotFoundException {
            getObjectCollection(collectionName);
            valueDAO.create(collectionName, objectValue);
            putToCacheObject(collectionName, objectValue);
    }

    private void putToCacheObject(final String collectionName, final ObjectValue objectValue)
            throws DaoException, NotFoundException {
        ICache<String, ObjectValue> cache = getCache(collectionName);
        if (cache == null) {
            ObjectCollection objectCollection = collectionDAO.read(collectionName);
            cache = objectCollection.getType().getCache(objectCollection.getMaxCount());
            cacheMap.put(collectionName, cache);
        }
        cache.put(objectValue.getName(), objectValue);
    }

    private ICache<String, ObjectValue> getCache(final String collectionName) {
        return cacheMap.get(collectionName);
    }

    /**
     * Read objectValue from cache or DB
     * @param collectionName name collection
     * @param valueName name objectValue
     * @return objectValue
     * @throws DaoException DAO exception
     * @throws NotFoundException if collection not found in DAO
     */
    public ObjectValue readObject(final String collectionName, final String valueName)
            throws DaoException, NotFoundException {
            ObjectValue objectValue = getCache(collectionName).get(valueName);
            if (objectValue == null) {
                return getObjectValue(collectionName, valueName);
            }
            return objectValue;
    }

    private ObjectValue getObjectValue(final String collectionName, final String valueName)
            throws DaoException, NotFoundException {
        ObjectValue objectValue;
        objectValue = valueDAO.read(collectionName, valueName);
        if (objectValue == null) {
            String notFoundValue = "Value not found.";
            throw new NotFoundException(notFoundValue);
        }
        return objectValue;
    }

    /**
     * Read all objects from DB
     * @param collectionName name collection
     * @return list of objectValue
     * @throws DaoException DAO exception
     */
    public List<ObjectValue> readObject(final String collectionName, final String count, final String id) throws DaoException {
            return valueDAO.read(collectionName, count, id);
    }

    /**
     * Replace objectValue in cache&DB
     * @param collectionName name collection
     * @param objectValue name objectValue
     * @throws DaoException DAO exception
     * @throws NotFoundException if collection not found in DAO
     */
    public void updateObject(final String collectionName,
                                          final ObjectValue objectValue) throws DaoException, NotFoundException {
            getObjectValue(collectionName, objectValue.getName());
            valueDAO.update(collectionName, objectValue);
            putToCacheObject(collectionName, objectValue);
    }

    /**
     * Remove objectValue from cache&DB
     * @param collectionName name collection
     * @param valueName name objectValue
     * @throws DaoException DAO exception
     */
    public void deleteObject(final String collectionName, final String valueName) throws DaoException {
            valueDAO.delete(collectionName, valueName);
            ICache<String, ObjectValue> cache = getCache(collectionName);
            if (cache != null) {
                cache.remove(valueName);
            }
    }

    /**
     * Update oe create object
     * @param collectionName Name collection
     * @param objectValue Object
     * @throws MergeConflictException Merge conflict exception
     * @throws DaoException DAO exception
     * @throws NotFoundException Collection not fount exception
     */
    public void updateOrCreateObject(final String collectionName, final ObjectValue objectValue) throws MergeConflictException, DaoException, NotFoundException {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        try {
            ObjectValue objectValueInDB = valueDAO.read(collectionName, objectValue.getName());
            if (objectValueInDB != null) {
                if (!objectValue.equals(objectValueInDB)) {
                    throw new MergeConflictException("Tried update object: " + objectValue.getName());
                }
            } else {
                valueDAO.create(collectionName, objectValue);
            }
            putToCacheObject(collectionName, objectValue);
        } finally {
            lock.unlock();
        }
    }


    private ObjectCollection getObjectCollection(final String collectionName) throws DaoException, NotFoundException {
        ObjectCollection obj = collectionDAO.read(collectionName);
        if (obj == null) {
            String notFoundCollection = "Collection not found in DB.";
            throw new NotFoundException(notFoundCollection);
        }
        return obj;
    }
}
