package by.training.services;

import by.training.beans.GroupNode;
import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.exceptions.NodeException;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import by.training.services.utils.HttpService;
import by.training.services.utils.ThreadExecutorService;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Service rebalance
 */
public class RebalanceService {
    private final Logger logger = LogManager.getLogger();
    private final String statusCode = "statusCode";
    private final String threadErrorMsg = "Thread error.";
    private final int countTry = 5;

    private final ICollectionDAO collectionDAO;
    private final IValueDAO valueDAO;
    private final ThreadExecutorService executor;
    private final List<GroupNode> servers;
    private final HttpService httpService;

    /**
     * Setter
     * @param isRebalanceCollection Rebalance collection or not
     */
    public void setIsRebalanceCollection(Boolean isRebalanceCollection) {
        this.isRebalanceCollection = isRebalanceCollection;
    }

    private Boolean isRebalanceCollection = false;
    private final String count = "100";

    /**
     * Getter
     * @return  Rebalance collection or not
     */
    public Boolean getIsRebalanceCollection() {
        return isRebalanceCollection;
    }

    /**
     * Constructor
     * @param collectionDAO Collection DAO
     * @param valueDAO Object DAO
     * @param executor Executor service
     * @param servers List groups server
     * @param httpService Http service
     */
    public RebalanceService(final ICollectionDAO collectionDAO, final IValueDAO valueDAO,
                            final ThreadExecutorService executor,
                            final List<GroupNode> servers, final HttpService httpService) {
        this.collectionDAO = collectionDAO;
        this.valueDAO = valueDAO;
        this.executor = executor;
        this.servers = servers;
        this.httpService = httpService;
    }

    /**
     * Rebalance collections
     * @throws DaoException DAO exception
     * @throws NodeException Exception if any node finished with error
     */
    public void rebalanceCollection() throws DaoException, NodeException {
        List<ObjectCollection> objectCollections = collectionDAO.read(count, Constants.EMPTY);
        while (objectCollections.size() > 0) {
            String lastCollection = objectCollections.get(objectCollections.size() - 1).getName();
            for (ObjectCollection objectCollection : objectCollections) {
                Gson gson = new Gson();
                Map<String, Future<Map<String, Object>>> futureMap = new HashMap<>();
                Map<String, Integer> resultMap = new HashMap<>();
                final String urlCollection = "/app/rebalanceCollection";
                List<String> wrongRequest = new ArrayList<>();
                for (GroupNode groupNode: servers) {
                        wrongRequest =
                                sendAndGetResult(objectCollection, groupNode, futureMap, resultMap, urlCollection);
                }
                if (wrongRequest.size() > 0) {
                    for (String address : wrongRequest) {
                        Boolean isRun = true;
                        int i = 0;
                        while (i < countTry) {
                            Future<Map<String, Object>> future = getFuture(gson.toJson(objectCollection),
                                    address, urlCollection);
                            try {
                                if ((Integer) future.get().get(statusCode) == HttpServletResponse.SC_OK) {
                                    isRun = false;
                                    break;
                                } else {
                                    i++;
                                }
                            } catch (ExecutionException e) {
                                logger.error(threadErrorMsg + ": " + e);
                                i++;
                            } catch (InterruptedException e) {
                                future.cancel(true);
                                logger.error(threadErrorMsg + ": " + e);
                                i++;
                            }
                        }
                        if (isRun) {
                            String message = "Tried " + i + "time send collection: \"" + objectCollection.getName()
                                    + "\" to server \"" + address + "\" and get bad request";
                            logger.error(message);
                            throw new NodeException(message);
                        }
                    }

                }
            }
            objectCollections = collectionDAO.read(count, lastCollection);
        }
        setIsRebalanceCollection(true);
    }

    private Future<Map<String, Object>> getFuture(final String json, final String address, final String url) {
        return executor.getExecutor().submit(
                () -> httpService.sendPut(address, url, json));
    }

    /**
     * Rebalance objects
     * @throws DaoException DAO exception
     * @throws NodeException Exception if any node finished with error
     */
    public void rebalanceObject() throws DaoException, NodeException {
        List<ObjectCollection> objectCollections = collectionDAO.read(count, Constants.EMPTY);
        while (objectCollections.size() > 0) {
            String lastCollection = objectCollections.get(objectCollections.size() - 1).getName();
            for (ObjectCollection objectCollection: objectCollections) {
                List<ObjectValue> objectValues = valueDAO.read(objectCollection.getName(), count, Constants.EMPTY);
                while (objectValues.size() > 0) {
                    String lastObject = objectValues.get(objectValues.size() - 1).getName();
                    for (ObjectValue objectValue: objectValues) {
                        GroupNode groupNode = getServerGroup(objectValue.getName());
                        Gson gson = new Gson();
                        Map<String, Future<Map<String, Object>>> futureMap = new HashMap<>();
                        Map<String, Integer> resultMap = new HashMap<>();
                        final String urlObject = "/app/rebalanceObject?collection=" + objectCollection.getName();
                        List<String> wrongRequest =
                                sendAndGetResult(objectValue, groupNode, futureMap, resultMap, urlObject);
                        if (wrongRequest.size() > 0) {
                            for (String address : wrongRequest) {
                                Boolean isRun = true;
                                int i = 0;
                                while (i < countTry) {
                                    Future<Map<String, Object>> future = getFuture(gson.toJson(objectValue),
                                            address, urlObject);
                                    try {
                                        if ((Integer) future.get().get(statusCode) == HttpServletResponse.SC_OK) {
                                            isRun = false;
                                            if (!groupNode.getIsMaster()) {
                                                valueDAO.delete(objectCollection.getName(), objectValue.getName());
                                            }
                                            break;
                                        } else {
                                            i++;
                                        }
                                    } catch (ExecutionException e) {
                                        logger.error(threadErrorMsg + ": " + e.getMessage());
                                        i++;
                                    } catch (InterruptedException e) {
                                        future.cancel(true);
                                        logger.error(threadErrorMsg + ": " + e.getMessage());
                                        i++;
                                    }
                                }
                                if (isRun) {
                                    String message = "Tried " + i + "time send object: \"" + objectValue.getName()
                                            + "\" to server \"" + address + "\" and got bad request";
                                    logger.error(message);
                                    throw new NodeException(message);
                                }
                            }
                        } else {
                            if (!groupNode.getIsMaster()) {
                                valueDAO.delete(objectCollection.getName(), objectValue.getName());
                            }
                        }
                    }
                    objectValues = valueDAO.read(objectCollection.getName(), count, lastObject);
                }
            }
            objectCollections = collectionDAO.read(count, lastCollection);
        }
    }

    private <T> List<String>  sendAndGetResult(final T object, final GroupNode groupNode,
                                               final Map<String, Future<Map<String, Object>>> futureMap,
                                               final Map<String, Integer> resultMap, final String url) {
        Gson gson = new Gson();
        groupNode.getServerNodes().stream()
                .filter(serverNode -> !serverNode.getMaster())
                .forEach((item -> {
                    Future<Map<String, Object>> future = getFuture(gson.toJson(object),
                            item.getAddress(), url);
                    futureMap.put(item.getAddress(), future);
                }));
        futureMap.forEach((address, integerFuture) -> {
            try {
                resultMap.put(address, (Integer) integerFuture.get().get(statusCode));
            } catch (ExecutionException e) {
                logger.error(threadErrorMsg + ": " + e.getMessage());
                resultMap.put(address, HttpServletResponse.SC_BAD_REQUEST);
            } catch (InterruptedException e) {
                integerFuture.cancel(true);
                logger.error(threadErrorMsg + ": " + e.getMessage());
                resultMap.put(address, HttpServletResponse.SC_BAD_REQUEST);
            }
        });
        return resultMap.entrySet().stream()
                .filter(entry -> entry.getValue() != HttpServletResponse.SC_OK)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private GroupNode getServerGroup(final String objectName) {
        Integer hash = Math.abs(objectName.hashCode() % servers.size());
        return servers.get(hash);
    }
}
