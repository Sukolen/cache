package by.training.services;

import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.exceptions.DaoException;
import by.training.exceptions.MergeConflictException;
import by.training.exceptions.NotFoundException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Collection service
 */
public class CollectionService {
    private final Map<String, ICache<String, ObjectValue>> cacheMap;
    private final ICollectionDAO collectionDAO;

    /**
     * Constructor
     * @param cacheMap map caches
     * @param collectionDAO collection DAO
     */

    public CollectionService(final Map<String, ICache<String, ObjectValue>> cacheMap,
                             final ICollectionDAO collectionDAO) {
        this.cacheMap = cacheMap;
        this.collectionDAO = collectionDAO;
    }

    /**
     * Create collection in DB&cache
     * @param objectCollection object collection (ObjectCollection)
     * @throws DaoException DAO exception
     */
    public void createCollection(final ObjectCollection objectCollection)
            throws DaoException {
        collectionDAO.create(objectCollection);
        cacheMap.put(objectCollection.getName(),
                objectCollection.getType().getCache(objectCollection.getMaxCount()));
    }

    public void updateOrCreateCollection(final ObjectCollection objectCollection) throws MergeConflictException, DaoException {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        try {
            ObjectCollection objectCollectionOnDB = collectionDAO.read(objectCollection.getName());
            if (objectCollectionOnDB != null) {
                if (!objectCollection.equals(objectCollectionOnDB)) {
                    throw new MergeConflictException("Tried update collection: " + objectCollection.getName());
                }
            } else {
                createCollection(objectCollection);
            }
            cacheMap.put(objectCollection.getName(),
                    objectCollection.getType().getCache(objectCollection.getMaxCount()));
        } finally {
            lock.unlock();
        }
    }

    /**
     * Read collection from DB
     * @param collectionName name collection
     * @return object collection (ObjectCollection)
     * @throws DaoException DAO exception
     * @throws NotFoundException if collection not found in DAO
     */
    public ObjectCollection readCollection(final String collectionName) throws DaoException, NotFoundException {
            return getObjectCollection(collectionName);
    }

    private ObjectCollection getObjectCollection(final String collectionName) throws DaoException, NotFoundException {
        ObjectCollection obj = collectionDAO.read(collectionName);
        if (obj == null) {
            String notFoundCollection = "Collection not found in DB.";
            throw new NotFoundException(notFoundCollection);
        }
        return obj;
    }

    /**
     * Read list of all collections from DB
     * @return list of object collection (ObjectCollection)
     * @throws DaoException DAO exception
     */
//    public List<ObjectCollection> readCollection() throws DaoException {
//            return collectionDAO.readAll();
//    }

    /**
     * Read list of collections from DB
     * @param count Count on page
     * @param id Search next after this id collection
     * @return list of object collection (ObjectCollection)
     * @throws DaoException DAO exception
     */
    public List<ObjectCollection> readCollection(final String count, final String id) throws DaoException {
        return collectionDAO.read(count, id);
    }

    /**
     * Replace collection from DB&cache
     * @param collectionName name collection
     * @param newCollection new collection
     * @throws DaoException DAO exception
     * @throws NotFoundException if collection not found in DAO
     */
    public void updateCollection(final String collectionName, final ObjectCollection newCollection)
            throws DaoException, NotFoundException {
            getObjectCollection(collectionName);
            collectionDAO.update(collectionName, newCollection.getName(),
                    newCollection.getType(), newCollection.getMaxCount());
            ICache<String, ObjectValue> cache = cacheMap.get(collectionName);
            ICache<String, ObjectValue> newCache = newCollection.getType().getCache(newCollection.getMaxCount());
            if (cache != null) {
                cache.copyCache(newCache);
                cacheMap.remove(collectionName);
            }
            cacheMap.put(newCollection.getName(), newCache);
    }


    /**
     * Remove collection from DB&cache
     * @param collectionName name collection
     * @throws DaoException DAO exception
     */
    public void deleteCollection(final String collectionName) throws DaoException {
            collectionDAO.delete(collectionName);
            cacheMap.remove(collectionName);
    }

}
