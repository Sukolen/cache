package by.training.services.node;

import by.training.beans.GroupNode;
import by.training.beans.ObjectValue;
import by.training.beans.ServerNode;
import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.exceptions.FatalErrorException;
import by.training.exceptions.NodeException;
import by.training.exceptions.NotFoundException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import by.training.services.ObjectService;
import by.training.services.utils.HealthCheckService;
import by.training.services.utils.HttpService;
import by.training.services.utils.JsonService;
import by.training.services.utils.ThreadExecutorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by Ruslan on 09.01.2018.
 */
public class NodeObjectService extends ObjectService {
    private final Logger logger = LogManager.getLogger();
    private final List<GroupNode> servers;
    private final ThreadExecutorService executor;
    private final HealthCheckService healthCheckService;
    private final String statusCode = "statusCode";
    private final String threadErrorMsg = "Thread error.";
    private final HttpService httpService;

    /**
     * Constructor
     * @param cacheMap      map caches
     * @param collectionDAO collection DAO
     * @param valueDAO      objectValue DAO
     * @param servers list of servers
     * @param executor thread pool service
     * @param healthCheckService HealthCheck service. Checking another nodes.
     * @param httpService HttpService
     */
    public NodeObjectService(final Map<String, ICache<String, ObjectValue>> cacheMap,
                             final ICollectionDAO collectionDAO,
                             final IValueDAO valueDAO,
                             final List<GroupNode> servers,
                             final ThreadExecutorService executor,
                             final HealthCheckService healthCheckService,
                             final HttpService httpService) {
        super(cacheMap, collectionDAO, valueDAO);
        this.servers = servers;
        this.executor = executor;
        this.healthCheckService = healthCheckService;
        this.httpService = httpService;
    }

    /**
     * Create object on node, which is selected by hash
     * @param collectionName Name collection
     * @param objectValue Object value
     * @param reqUrl Request url
     * @param isMaster Parameter which say, request from master node or not
     * @throws DaoException DAO exception
     * @throws NotFoundException If collection not found in DAO
     * @throws NodeException Exception if any node finished with error
     */
    public void createObject(final String collectionName, final ObjectValue objectValue, final String reqUrl,
                             final Boolean isMaster) throws DaoException, NotFoundException, NodeException, FatalErrorException {
        if (!isMaster) {
            List<ServerNode> serverNodeList = getServerGroup(objectValue.getName()).getServerNodes();
            healthCheckService.checkServersGroup(serverNodeList);
            Map<String, Integer> resultMap;
            Map<String, Future<Map<String, Object>>> futureMap = new HashMap<>();
            serverNodeList.stream().filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                Future<Map<String, Object>> future = executor.getExecutor().submit(
                        () -> httpService.sendPost(item.getAddress(), reqUrl, objectValue.getValue()));
                futureMap.put(item.getAddress(), future);
            }));

            resultMap = setResultMap(futureMap);

            List<String> wrongRequest = resultMap.entrySet().stream()
                    .filter(entry -> entry.getValue() != HttpServletResponse.SC_CREATED)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            if (wrongRequest.size() > 0) {
                futureMap.clear();
                resultMap.entrySet().stream()
                        .filter(entry -> entry.getValue() == HttpServletResponse.SC_CREATED)
                        .forEach((entry) -> {
                            Future<Map<String, Object>> future = executor.getExecutor().submit(
                                    () -> httpService.sendDelete(entry.getKey(),
                                            "/app/collection/" + collectionName + "/" + objectValue.getName()));
                            futureMap.put(entry.getKey(), future);
                        });

                List<String> wrongRollbackRequest = getWrongResultList(futureMap, HttpServletResponse.SC_OK);
                String message = "Node '" + wrongRequest.toString() + "' is response bad result.";
                if (wrongRollbackRequest.size() > 0) {
                    message = message + " And node '" + wrongRollbackRequest.toString() + "' isn't rollback.";
                    throw new FatalErrorException(message);
                } else {
                    throw new NodeException(message);
                }

            } else {
                if (serverNodeList.stream().anyMatch(ServerNode::getMaster)) {
                    super.createObject(collectionName, objectValue);
                }
            }

        } else {
            super.createObject(collectionName, objectValue);
        }
    }

    private GroupNode getServerGroup(final String objectName) {
        int hash = Math.abs(objectName.hashCode() % servers.size());
        return servers.get(hash);
    }

    /**
     * Read object value on node, which is selected by hash
     * @param collectionName Name collection
     * @param valueName Name value
     * @param reqUrl Request url
     * @param isMaster Parameter which say, request from master node or not
     * @return Object value
     * @throws DaoException DAO exception
     * @throws NotFoundException If collection not found in DAO
     * @throws NodeException Exception if any node finished with error
     */
    public ObjectValue readObject(final String collectionName, final String valueName, final String reqUrl,
                                  final Boolean isMaster) throws DaoException, NotFoundException, NodeException {
        if (!isMaster) {
            GroupNode groupNode = getServerGroup(valueName);
            ServerNode serverNode = getLifeServerNode(groupNode);
            if (serverNode.getMaster()) {
                return super.readObject(collectionName, valueName);
            } else {
                healthCheckService.checkServer(serverNode);
                Map<String, Object> response = httpService.sendGet(serverNode.getAddress(), reqUrl);
                if ((Integer) response.get(statusCode) != HttpServletResponse.SC_OK) {
                    throw new NodeException("Node '" + serverNode.getAddress() + "' is response bad result.");
                }
                return JsonService.getObjectFromJSON(response.get(Constants.RESPONSE_BODY).toString());
            }
        } else {
            return super.readObject(collectionName, valueName);
        }
    }

    private ServerNode getLifeServerNode(final GroupNode groupNode) throws NodeException {
        ServerNode serverNode = groupNode.getNextServer();
        int count = 0;
        while ((!serverNode.getHealth()) && count < groupNode.getServerNodes().size()) {
            serverNode = groupNode.getNextServer();
            count++;
        }
        if (count == groupNode.getServerNodes().size()) {
            throw new NodeException("All servers not available. Servers: " + groupNode.getServerNodes());
        }
        return serverNode;
    }

    /**
     *  Delete object value on node, which is selected by hash
     * @param collectionName Name collection
     * @param valueName Name object value
     * @param reqUrl Request url
     * @param isMaster Parameter which say, request from master node or not
     * @throws DaoException DAO exception
     * @throws NodeException Exception if any node finished with error
     */
    public void deleteObject(final String collectionName, final String valueName, final String reqUrl,
                             final Boolean isMaster) throws DaoException, NodeException {
        if (!isMaster) {
            List<ServerNode> serverNodeList = getServerGroup(valueName).getServerNodes();
            healthCheckService.checkServersGroup(serverNodeList);
            Map<String, Future<Map<String, Object>>> futureMap = new HashMap<>();

            serverNodeList.stream().filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                Future<Map<String, Object>> future = executor.getExecutor().submit(
                        () -> httpService.sendDelete(item.getAddress(), reqUrl));
                futureMap.put(item.getAddress(), future);
            }));

            List<String> wrongRequest = getWrongResultList(futureMap, HttpServletResponse.SC_OK);
            if (wrongRequest.size() > 0) {
                String message = "Node '" + wrongRequest.toString() + "' is response bad result.";
                throw new NodeException(message);
            } else {
                if (serverNodeList.stream().anyMatch(ServerNode::getMaster)) {
                    super.deleteObject(collectionName, valueName);
                }
            }
        } else {
            super.deleteObject(collectionName, valueName);
        }
    }

    /**
     * Update object value on node, which is selected by hash
     * @param collectionName Name collection
     * @param objectValue Object value
     * @param reqUrl Request url
     * @param isMaster Parameter which say, request from master node or not
     * @throws DaoException DAO exception
     * @throws NotFoundException If collection not found in DAO
     * @throws NodeException Exception if any node finished with error
     */
    public void updateObject(final String collectionName, final ObjectValue objectValue, final String reqUrl,
                             final Boolean isMaster) throws DaoException, NotFoundException, NodeException, FatalErrorException {
        if (!isMaster) {
            List<ServerNode> serverNodeList = getServerGroup(objectValue.getName()).getServerNodes();
            healthCheckService.checkServersGroup(serverNodeList);
            ServerNode nextServerNode = getServerGroup(objectValue.getName()).getNextServer();
            ObjectValue oldObjectValue;
            if (nextServerNode.getMaster()) {
                oldObjectValue = super.readObject(collectionName, objectValue.getName());
            } else {
                Map<String, Object> result = httpService.sendGet(nextServerNode.getAddress(), reqUrl);
                String json;
                if ((Integer) result.get(statusCode) == HttpServletResponse.SC_OK) {
                    json = (String) result.get(Constants.RESPONSE_BODY);
                    oldObjectValue = JsonService.getObjectFromJSON(json);
                } else {
                    throw new NodeException("Node '" + nextServerNode.getAddress() + "' is response bad result.");
                }
            }
            Map<String, Integer> resultMap;
            Map<String, Future<Map<String, Object>>> futureMap = new HashMap<>();
            serverNodeList.stream().filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                Future<Map<String, Object>> future = executor.getExecutor().submit(
                        () -> httpService.sendPut(item.getAddress(), reqUrl, objectValue.getValue()));
                futureMap.put(item.getAddress(), future);
            }));

            resultMap = setResultMap(futureMap);

            List<String> wrongRequest = resultMap.entrySet().stream()
                    .filter(entry -> entry.getValue() != HttpServletResponse.SC_OK)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            if (wrongRequest.size() > 0) {
                futureMap.clear();
                resultMap.entrySet().stream()
                        .filter(entry -> entry.getValue() == HttpServletResponse.SC_OK)
                        .forEach((entry) -> {
                            Future<Map<String, Object>> future = executor.getExecutor().submit(
                                    () -> httpService.sendPut(entry.getKey(),
                                            "/app/collection/" + collectionName + "/" + objectValue.getName(),
                                            oldObjectValue.getValue()));
                            futureMap.put(entry.getKey(), future);
                        });

                List<String> wrongRollbackRequest = getWrongResultList(futureMap, HttpServletResponse.SC_OK);
                String message = "Node '" + wrongRequest.toString() + "' is response bad result.";
                if (wrongRollbackRequest.size() > 0) {
                    message = message + " And node '" + wrongRollbackRequest.toString() + "' isn't rollback.";
                    throw new FatalErrorException(message);
                } else {
                    throw new NodeException(message);
                }
            } else {
                if (serverNodeList.stream().anyMatch(ServerNode::getMaster)) {
                    super.updateObject(collectionName, objectValue);
                }
            }
        } else {
            super.updateObject(collectionName, objectValue);
        }
    }

    private Map<String, Integer> setResultMap(final Map<String, Future<Map<String, Object>>> futureMap) {
        Map<String, Integer> resultMap = new HashMap<>();
        futureMap.forEach((address, integerFuture) -> {
            try {
                resultMap.put(address, (Integer) integerFuture.get().get(statusCode));
            } catch (ExecutionException e) {
                logger.error(threadErrorMsg + ": " + e.getCause());
                resultMap.put(address, HttpServletResponse.SC_BAD_REQUEST);
            } catch (InterruptedException e) {
                integerFuture.cancel(true);
                logger.error(threadErrorMsg + ": " + e.getCause());
                resultMap.put(address, HttpServletResponse.SC_BAD_REQUEST);
            }
        });
        return resultMap;
    }


    /**
     * Read objects value from all nodes
     * @param collectionName Name collection
     * @param reqUrl Request url
     * @param isMaster Parameter which say, request from master node or not
     * @param count Count on page
     * @param id Name object
     * @return List objects value
     * @throws DaoException DAO exception
     * @throws NodeException Exception if any node finished with error
     */
    public List<ObjectValue> readObjects(final String collectionName, final String reqUrl,
                                         final Boolean isMaster, final String count, final String id)
            throws DaoException, NodeException {
        List<ObjectValue> objectValues = new ArrayList<>();
        if (!isMaster) {
            healthCheckService.checkServers();
            Map<String, Future<Map<String, Object>>> futureMap = new HashMap<>();
            for (GroupNode groupNode: servers) {
                ServerNode serverNode = getLifeServerNode(groupNode);
                if (serverNode.getMaster()) {
                    objectValues.addAll(super.readObject(collectionName, count, id));
                } else {
                    Future<Map<String, Object>> future = executor.getExecutor().submit(
                            () -> httpService.sendGet(serverNode.getAddress(), reqUrl));
                    futureMap.put(serverNode.getAddress(), future);
                }
            }

            for (Map.Entry<String, Future<Map<String, Object>>> entry : futureMap.entrySet()) {
                String json = null;
                try {
                    json = (String) entry.getValue().get().get(Constants.RESPONSE_BODY);
                } catch (ExecutionException e) {
                    logger.error(threadErrorMsg + ": " + e);
                } catch (InterruptedException e) {
                    entry.getValue().cancel(true);
                    logger.error(threadErrorMsg + ": " + e.getCause());
                }
                if (json == null) {
                    throw new NodeException("Node '" + entry.getKey() + "' is response bad result.");
                } else {
                    objectValues.addAll(JsonService.getObjectsFromJson(json));
                }
            }
        } else {
            objectValues = super.readObject(collectionName, count, id);
        }
        return objectValues.stream()
                .sorted(Comparator.comparing(ObjectValue::getName))
                .limit(Integer.valueOf(count))
                .collect(Collectors.toList());
    }

    private List<String> getWrongResultList(final Map<String, Future<Map<String, Object>>> futureMap, final int goodResult) {
        Map<String, Integer> resultMap;
        resultMap = setResultMap(futureMap);
        return resultMap.entrySet().stream()
                .filter(entry -> entry.getValue() != goodResult)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

}
