package by.training.services.node;

import by.training.beans.GroupNode;
import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.beans.ServerNode;
import by.training.exceptions.DaoException;
import by.training.exceptions.FatalErrorException;
import by.training.exceptions.NodeException;
import by.training.exceptions.NotFoundException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.services.CollectionService;
import by.training.services.utils.HealthCheckService;
import by.training.services.utils.HttpService;
import by.training.services.utils.JsonService;
import by.training.services.utils.ThreadExecutorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Created by Ruslan on 23.01.2018.
 */
public class NodeCollectionService extends CollectionService {
    private final Logger logger = LogManager.getLogger();
    private final ThreadExecutorService executor;
    private final HealthCheckService healthCheckService;
    private final List<ServerNode> servers = new ArrayList<>();
    private final String statusCode = "statusCode";
    private final String threadErrorMsg = "Thread error.";
    private final HttpService httpService;

    /**
     * Constructor
     * @param cacheMap      map caches
     * @param collectionDAO collection DAO
     * @param executor thread pool service
     * @param healthCheckService HealthCheck service. Checking another nodes.
     * @param servers list of servers
     * @param httpService HttpService
     */
    public NodeCollectionService(final Map<String, ICache<String, ObjectValue>> cacheMap,
                                 final ICollectionDAO collectionDAO,
                                 final ThreadExecutorService executor,
                                 final HealthCheckService healthCheckService,
                                 final List<GroupNode> servers,
                                 final HttpService httpService) {
        super(cacheMap, collectionDAO);
        this.executor = executor;
        this.healthCheckService = healthCheckService;
        this.httpService = httpService;
        servers.stream()
                .forEach(groupNode -> groupNode.getServerNodes().stream()
                        .forEach(this.servers::add));
    }

    /**
     * Update collections on all nodes
     * @param collectionName Collection name
     * @param newCollection New collection
     * @param isMaster Parameter which say, request from master node or not
     * @param reqUrl Request url
     * @param body Body request
     * @throws DaoException DAO exception
     * @throws NotFoundException If collection not found in DAO
     * @throws NodeException Exception if any node finished with error
     * @throws FatalErrorException After this exception, server can't working
     */
    public void updateCollection(final String collectionName, final ObjectCollection newCollection,
                                 final Boolean isMaster, final String reqUrl, final String body)
            throws DaoException, NotFoundException, NodeException, FatalErrorException {
        if (!isMaster) {
            healthCheckService.checkServers();
            ObjectCollection oldCollection = super.readCollection(collectionName);
            Map<String, Future<Map<String, Object>>> futureMap = new LinkedHashMap<>();
            Map<String, Integer> resultMap;
            servers.stream().filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                Future<Map<String, Object>> future = executor.getExecutor().submit(
                        () -> httpService.sendPut(item.getAddress(), reqUrl, body));
                futureMap.put(item.getAddress(), future);
            }));

            resultMap = setResultMap(futureMap);

            List<String> wrongRequest = resultMap.entrySet().stream()
                    .filter(entry -> entry.getValue() != HttpServletResponse.SC_OK)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            if (wrongRequest.size() > 0) {
                futureMap.clear();
                resultMap.entrySet().stream()
                        .filter(entry -> entry.getValue() == HttpServletResponse.SC_OK)
                        .forEach((entry) -> {
                            Future<Map<String, Object>> future = executor.getExecutor().submit(
                                    () -> httpService.sendPut(entry.getKey(),
                                            "/app/collections/" + newCollection.getName(),
                                            JsonService.getJSON(oldCollection)));
                            futureMap.put(entry.getKey(), future);
                        });

                List<String> wrongRollbackRequest = getWrongResultList(futureMap, HttpServletResponse.SC_OK);
                String message = "Tried update collection \"" + oldCollection.getName() + "\", but node '" + wrongRequest.toString() + "' is response bad result.";
                if (wrongRollbackRequest.size() > 0) {
                    message = message + " And node '" + wrongRollbackRequest.toString() + "' isn't rollback.";
                    throw new FatalErrorException(message);
                } else {
                    throw new NodeException(message);
                }

            } else {
                super.updateCollection(collectionName, newCollection);
            }
        } else {
            super.updateCollection(collectionName, newCollection);
        }
    }

    /**
     * Delete collection from all nodes
     * @param collectionName Name collection
     * @param isMaster Parameter which say, request from master node or not
     * @param reqUrl Request url
     * @throws DaoException DAO exception
     * @throws NodeException Exception if any node finished with error
     */
    public void deleteCollection(final String collectionName, final Boolean isMaster, final String reqUrl)
            throws DaoException, NodeException {
        if (!isMaster) {
            healthCheckService.checkServers();
            Map<String, Future<Map<String, Object>>> futureMap = new LinkedHashMap<>();
            servers.stream().filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                Future<Map<String, Object>> future = executor.getExecutor().submit(
                        () -> httpService.sendDelete(item.getAddress(), reqUrl));
                futureMap.put(item.getAddress(), future);
            }));

            List<String> wrongRequest = getWrongResultList(futureMap, HttpServletResponse.SC_OK);
            if (wrongRequest.size() > 0) {
                String message = "Node '" + wrongRequest.toString() + "' is response bad result.";
                throw new NodeException(message);
            } else {
                super.deleteCollection(collectionName);
            }
        } else {
            super.deleteCollection(collectionName);
        }
    }

    /**
     * Create collection on all nodes
     * @param objectCollection Collection object
     * @param isMaster Parameter which say, request from master node or not
     * @param reqUrl Request url
     * @param body Request body
     * @throws DaoException DAO exception
     * @throws NodeException Exception if any node finished with error
     * @throws FatalErrorException After this exception, server can't working
     */
    public void createCollection(final ObjectCollection objectCollection,
                                 final Boolean isMaster, final String reqUrl, final String body)
            throws DaoException, NodeException, FatalErrorException {
        if (!isMaster) {
            healthCheckService.checkServers();
            Map<String, Future<Map<String, Object>>> futureMap = new LinkedHashMap<>();
            Map<String, Integer> resultMap;
            servers.stream().filter(serverNode -> !serverNode.getMaster()).forEach((item -> {
                Future<Map<String, Object>> future = executor.getExecutor().submit(
                        () -> httpService.sendPost(item.getAddress(), reqUrl, body));
                futureMap.put(item.getAddress(), future);
            }));

            resultMap = setResultMap(futureMap);

            List<String> wrongRequest = resultMap.entrySet().stream()
                    .filter(entry -> entry.getValue() != HttpServletResponse.SC_CREATED)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            if (wrongRequest.size() > 0) {
                futureMap.clear();
                resultMap.entrySet().stream()
                        .filter(entry -> entry.getValue() == HttpServletResponse.SC_CREATED)
                        .forEach((entry) -> {
                            Future<Map<String, Object>> future = executor.getExecutor().submit(
                                    () -> httpService.sendDelete(entry.getKey(),
                                            "/app/collections/" + objectCollection.getName()));
                            futureMap.put(entry.getKey(), future);
                        });

                List<String> wrongRollbackRequest = getWrongResultList(futureMap, HttpServletResponse.SC_OK);
                String message = "Node '" + wrongRequest.toString() + "' is response bad result.";
                if (wrongRollbackRequest.size() > 0) {
                    message = message + " And node '" + wrongRollbackRequest.toString() + "' isn't rollback.";
                    throw new FatalErrorException(message);
                } else {
                    throw new NodeException(message);
                }

            } else {
                super.createCollection(objectCollection);
            }

        } else {
            super.createCollection(objectCollection);
        }
    }

    private Map<String, Integer> setResultMap(Map<String, Future<Map<String, Object>>> futureMap) {
        Map<String, Integer> resultMap = new HashMap<>();
        futureMap.forEach((address, integerFuture) -> {
            try {
                resultMap.put(address, (Integer) integerFuture.get().get(statusCode));
            } catch (ExecutionException e) {
                logger.error(threadErrorMsg + ": " + e.getCause());
                resultMap.put(address, HttpServletResponse.SC_BAD_REQUEST);
            } catch (InterruptedException e) {
                integerFuture.cancel(true);
                logger.error(threadErrorMsg + ": " + e.getCause());
                resultMap.put(address, HttpServletResponse.SC_BAD_REQUEST);
            }
        });
        return resultMap;
    }

    private List<String> getWrongResultList(final Map<String, Future<Map<String, Object>>> futureMap, final int goodResult) {
        Map<String, Integer> resultMap;
        resultMap = setResultMap(futureMap);

        return resultMap.entrySet().stream()
                .filter(entry -> entry.getValue() != goodResult)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }



}
