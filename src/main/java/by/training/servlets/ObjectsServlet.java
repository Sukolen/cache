package by.training.servlets;

import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.exceptions.FatalErrorException;
import by.training.exceptions.NodeException;
import by.training.exceptions.NotFoundException;
import by.training.services.node.NodeCollectionService;
import by.training.services.utils.JsonService;
import by.training.services.node.NodeObjectService;
import org.everit.json.schema.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Flash on 05.12.2017.
 */
public class ObjectsServlet extends AbstractServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = (String) req.getAttribute(Constants.SERVLET_PARAM_COLLECTION);
        String nameObject = (String) req.getAttribute(Constants.SERVLET_PARAM_OBJECT);
        if (Constants.EMPTY.equals(nameCollection)) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        NodeObjectService cacheValue = getService(Constants.OBJECT_NODE_PARAM);
        String url = req.getContextPath() + req.getRequestURI();
        if (!"".equals(nameObject)) {
            ObjectValue objectValue;
            try {
                Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
                objectValue = cacheValue.readObject(nameCollection, nameObject, url, isMaster);
            } catch (DaoException e) {
                resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
                return;
            } catch (NotFoundException e) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
                return;
            } catch (NodeException e) {
                getLogger().error(e.getMessage());
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
                return;
            }
            String jsonObject = JsonService.getJSON(objectValue);
            sendJson(resp, jsonObject);
        } else {
            List<ObjectValue> objectValues;
            try {
                Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
                String count = (String) req.getAttribute(Constants.COUNT_PARAM);
                String id = (String) req.getAttribute(Constants.ID_PARAM);
                objectValues = cacheValue.readObjects(nameCollection, url, isMaster, count, id);
            } catch (DaoException e) {
                resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
                return;
            } catch (NodeException e) {
                getLogger().error(e.getMessage());
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
                return;
            }
            String jsonObjects = JsonService.getJSONFromObjects(objectValues);
            sendJson(resp, jsonObjects);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = (String) req.getAttribute(Constants.SERVLET_PARAM_COLLECTION);
        String nameObject = (String) req.getAttribute(Constants.SERVLET_PARAM_OBJECT);
        if ("".equals(nameCollection)) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        StringBuilder stringBuilder = getStringBuilder(req, resp);

        NodeObjectService objectService = getService(Constants.OBJECT_NODE_PARAM);
        try {
            NodeCollectionService collectionService = (NodeCollectionService) getServletContext().getAttribute(Constants.COLLECTION_NODE_PARAM);
            ObjectCollection objectCollection = collectionService.readCollection(nameCollection);
            String jsonObject = stringBuilder.toString();
            if (JsonService.validateJSON(objectCollection.getJsonSchema(), jsonObject)) {
                ObjectValue objectValue;
                String url = req.getContextPath() + req.getRequestURI();
                if (!"".equals(nameObject)) {
                    objectValue = new ObjectValue(nameObject, jsonObject);
                } else {
                    String id = UUID.randomUUID().toString();
                    objectValue = new ObjectValue(id, jsonObject);
                    sendJson(resp, "{\"name\":\"" + id + "\"}");
                    url = url + "/" + id;
                }
                Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
                objectService.createObject(nameCollection, objectValue, url, isMaster);
            }
        } catch (DaoException | ValidationException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
            return;
        } catch (NotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
            return;
        } catch (NodeException e) {
            getLogger().error(e.getMessage());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            return;
        } catch (FatalErrorException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            getLogger().fatal(e.getMessage());
            System.exit(-1);
            return;
        }
        resp.setStatus(HttpServletResponse.SC_CREATED);
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = (String) req.getAttribute(Constants.SERVLET_PARAM_COLLECTION);
        String nameObject = (String) req.getAttribute(Constants.SERVLET_PARAM_OBJECT);
        if ((Constants.EMPTY.equals(nameCollection)) || (Constants.EMPTY.equals(nameObject))) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        StringBuilder stringBuilder = getStringBuilder(req, resp);

        NodeObjectService cacheValue = getService(Constants.OBJECT_NODE_PARAM);
        try {
            ObjectValue objectValue = new ObjectValue(nameObject, stringBuilder.toString());
            String url = req.getContextPath() + req.getRequestURI();
            Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
            cacheValue.updateObject(nameCollection, objectValue, url, isMaster);
        } catch (DaoException | IllegalArgumentException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (NotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
        } catch (NodeException e) {
            getLogger().error(e.getMessage());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } catch (FatalErrorException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            getLogger().fatal(e.getMessage());
            System.exit(-1);
        }

    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = (String) req.getAttribute(Constants.SERVLET_PARAM_COLLECTION);
        String nameObject = (String) req.getAttribute(Constants.SERVLET_PARAM_OBJECT);
        if ((Constants.EMPTY.equals(nameCollection)) || (Constants.EMPTY.equals(nameObject))) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        NodeObjectService cacheValue = getService(Constants.OBJECT_NODE_PARAM);
        try {
            String url = req.getContextPath() + req.getRequestURI();
            Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
            cacheValue.deleteObject(nameCollection, nameObject, url, isMaster);
        } catch (DaoException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (NodeException e) {
            getLogger().error(e.getMessage());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
    }
}
