package by.training.servlets;

import by.training.beans.ObjectCollection;
import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.exceptions.MergeConflictException;
import by.training.exceptions.NotFoundException;
import by.training.services.node.NodeCollectionService;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Ruslan on 18.02.2018.
 */
public class RebalanceCollectionServlet extends AbstractServlet {
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NodeCollectionService collectionService = getService(Constants.COLLECTION_NODE_PARAM);
        StringBuilder stringBuilder = getStringBuilder(req, resp);
        Gson gson = new Gson();
        ObjectCollection objectCollection = gson.fromJson(stringBuilder.toString(), ObjectCollection.class);
        try {
            collectionService.updateOrCreateCollection(objectCollection);
        } catch (MergeConflictException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Merge conflict.");
            getLogger().fatal(e.getMessage());
            System.exit(-1);
        } catch (DaoException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }
}
