package by.training.servlets;

import by.training.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by Flash on 22.02.2018.
 */
public abstract class AbstractServlet extends HttpServlet {
    protected Logger getLogger() {
        return logger;
    }

    private final Logger logger = LogManager.getLogger();
    protected StringBuilder getStringBuilder(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try (BufferedReader reader = req.getReader()) {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return stringBuilder;
    }

    protected void sendJson(HttpServletResponse resp, String json) throws IOException {
        resp.setContentType(Constants.CONTENT_TYPE_JSON);
        resp.getWriter().write(json);
    }

    protected <T> T getService(String service) {
        return (T) getServletContext().getAttribute(service);
    }
}
