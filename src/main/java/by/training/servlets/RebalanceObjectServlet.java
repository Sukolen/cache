package by.training.servlets;

import by.training.beans.ObjectValue;
import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.exceptions.MergeConflictException;
import by.training.exceptions.NotFoundException;
import by.training.services.node.NodeObjectService;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Ruslan on 21.02.2018.
 */
public class RebalanceObjectServlet  extends AbstractServlet {
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NodeObjectService objectService = getService(Constants.OBJECT_NODE_PARAM);
        StringBuilder stringBuilder = getStringBuilder(req, resp);
        Gson gson = new Gson();
        ObjectValue objectValue = gson.fromJson(stringBuilder.toString(), ObjectValue.class);
        String collectionName = req.getParameter("collection");
        try {
            objectService.updateOrCreateObject(collectionName, objectValue);
        } catch (MergeConflictException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Merge conflict.");
            getLogger().fatal(e.getMessage());
            System.exit(-1);
        } catch (DaoException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (NotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
        }
    }
}
