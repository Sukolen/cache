package by.training.servlets;

import by.training.beans.GroupNode;
import by.training.beans.ServerNode;
import by.training.constants.Constants;
import by.training.services.utils.JsonService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Ruslan on 14.01.2018.
 */
public class HealthCheck extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String dateTimeJsonElem = "dateTime";
        final String isRebalanceJsonElem = "isRebalanceCollection";
        List<GroupNode> groupNodes = (List<GroupNode>) getServletContext().getAttribute(Constants.GROUPS_PARAM);

        List<ServerNode> serverNodes = groupNodes.stream()
                .flatMap(groupNode1 -> groupNode1.getServerNodes().stream())
                .collect(Collectors.toList());

        JsonArray json = JsonService.getJsonArray(serverNodes);
        json.forEach(jsonElement -> {
            ((JsonObject) jsonElement).remove(dateTimeJsonElem);
            ((JsonObject) jsonElement).remove(isRebalanceJsonElem);
        });
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.getWriter().write(json.toString());
    }
}
