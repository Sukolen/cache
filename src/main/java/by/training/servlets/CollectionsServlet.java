package by.training.servlets;

import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.beans.ObjectCollection;
import by.training.exceptions.FatalErrorException;
import by.training.exceptions.NodeException;
import by.training.exceptions.NotFoundException;
import by.training.services.node.NodeCollectionService;
import by.training.services.utils.JsonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by Flash on 30.11.2017.
 */
public class CollectionsServlet extends AbstractServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NodeCollectionService collectionService = getService(Constants.COLLECTION_NODE_PARAM);
        String nameCollection = (String) req.getAttribute(Constants.SERVLET_PARAM_COLLECTION);
        if (!Constants.EMPTY.equals(nameCollection)) {
            ObjectCollection objectCollection;
            try {
                objectCollection = collectionService.readCollection(nameCollection);
            } catch (DaoException e) {
                resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
                return;
            } catch (NotFoundException e) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
                return;
            }
            String jsonCollection = JsonService.getJSON(objectCollection);
            sendJson(resp, jsonCollection);
        } else {
            List<ObjectCollection> objectCollections;
            try {
                String count = (String) req.getAttribute(Constants.COUNT_PARAM);
                String id = (String) req.getAttribute(Constants.ID_PARAM);
                objectCollections = collectionService.readCollection(count, id);
            } catch (DaoException e) {
                resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
                return;
            }
            String jsonCollections = JsonService.getJSONFromCollections(objectCollections);
            sendJson(resp, jsonCollections);
        }

    }

    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = (String) req.getAttribute(Constants.SERVLET_PARAM_COLLECTION);
        if (Constants.EMPTY.equals(nameCollection)) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        StringBuilder stringBuilder = getStringBuilder(req, resp);
        NodeCollectionService collectionService = getService(Constants.COLLECTION_NODE_PARAM);
        try {
            ObjectCollection objectCollection = JsonService
                    .getCollectionFromJSONWithoutSchema(stringBuilder.toString());
            Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
            String url = req.getContextPath() + req.getRequestURI();
            collectionService.updateCollection(nameCollection, objectCollection, isMaster, url, stringBuilder.toString());
        } catch (DaoException | IllegalArgumentException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (NotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
        } catch (NodeException e) {
            getLogger().error(e.getMessage());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } catch (FatalErrorException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            getLogger().fatal(e.getMessage());
            System.exit(-1);
        }

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = req.getAttribute(Constants.SERVLET_PARAM_COLLECTION).toString();
        if (Constants.EMPTY.equals(nameCollection)) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        StringBuilder stringBuilder = getStringBuilder(req, resp);

        NodeCollectionService collectionService = getService(Constants.COLLECTION_NODE_PARAM);
        try {
            ObjectCollection objectCollection = JsonService.getCollectionFromJSON(nameCollection, stringBuilder.toString());
            Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
            String url = req.getContextPath() + req.getRequestURI();
            collectionService.createCollection(objectCollection, isMaster, url, stringBuilder.toString());
        } catch (DaoException | IllegalArgumentException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
            return;
        } catch (NodeException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            return;
        } catch (FatalErrorException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            getLogger().fatal(e.getMessage());
            System.exit(-1);
            return;
        }
        resp.setStatus(HttpServletResponse.SC_CREATED);

    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameCollection = req.getAttribute(Constants.SERVLET_PARAM_COLLECTION).toString();
        if (Constants.EMPTY.equals(nameCollection)) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }
        NodeCollectionService collectionService = getService(Constants.COLLECTION_NODE_PARAM);
        try {
            Boolean isMaster = Boolean.valueOf(req.getHeader(Constants.SERVLET_PARAM_MASTER));
            String url = req.getContextPath() + req.getRequestURI();
            collectionService.deleteCollection(nameCollection, isMaster, url);
        } catch (DaoException e) {
            resp.sendError(Constants.HTTP_UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (NodeException e) {
            getLogger().error(e.getMessage());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }

    }
}
