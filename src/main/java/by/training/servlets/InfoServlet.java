package by.training.servlets;

import by.training.constants.Constants;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Flash on 12.01.2018.
 */
public class InfoServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        Gson gson = new Gson();
        String json = gson.toJson(getServletContext().getAttribute(Constants.IS_REBALANCE_COLLECTION_PARAM));
        response.getWriter().write(json);
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
