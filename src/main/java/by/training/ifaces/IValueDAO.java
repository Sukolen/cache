package by.training.ifaces;

import by.training.exceptions.DaoException;
import by.training.beans.ObjectValue;
//import by.training.reflect.AclLog;

import java.util.List;

/**
 * Interface for DAO value
 */
public interface IValueDAO extends IDAO {
    void create(String collectionName, ObjectValue objectValue) throws DaoException;
    ObjectValue read(String collectionName, String valueName) throws DaoException;
    //List<ObjectValue> read(String collectionName) throws DaoException;
    List<ObjectValue> read(String collectionName, final String count, final String id) throws DaoException;
    void update(String collectionName, ObjectValue newObjectValue) throws DaoException;
    void delete(String collectionName,  String valueName) throws DaoException;
}
