package by.training.ifaces;

import java.util.List;

/**
 * Interface for cache
 * @param <K> key
 * @param <V> value
 */
public interface ICache<K, V> {
    int size();
    V get(Object key);
    V put(K key, V value);
    V remove(Object key);
    boolean containsKey(Object key);
    List<V> getAll();
    ICache copyCache(ICache cache);
}
