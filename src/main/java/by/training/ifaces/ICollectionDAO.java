package by.training.ifaces;

import by.training.exceptions.DaoException;
import by.training.beans.ObjectCollection;
import by.training.enums.TypeCache;
import by.training.exceptions.MergeConflictException;

import java.util.List;

/**
 * Interface for DAO collection
 */
public interface ICollectionDAO extends IDAO {
    void create(ObjectCollection collection) throws DaoException;
    ObjectCollection read(String name) throws DaoException;
    List<ObjectCollection> read(String count, String id) throws DaoException;
    void update(String name, String newName, TypeCache typeCache, int maxCount) throws DaoException;
    void delete(String name) throws DaoException;
}
