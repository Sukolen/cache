package by.training.constants;

/**
 * Created by Flash on 20.01.2018.
 */
public class Constants {
    public static final Integer HTTP_UNPROCESSABLE_ENTITY = 422;
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String SERVLET_PARAM_COLLECTION = "collectionName";
    public static final String SERVLET_PARAM_OBJECT = "objectName";
    public static final String SERVLET_PARAM_MASTER = "master";
    public static final String EMPTY = "";
    public static final String OBJECT_NODE_PARAM = "objectNodeService";
    public static final String COLLECTION_NODE_PARAM = "collectionNodeService";
    public static final String SERVERS_PARAM =  "servers";
    public static final String GROUPS_PARAM =  "groups";
    public static final String RESPONSE_BODY = "responseBody";
    public static final String COUNT_PARAM = "count";
    public static final String ID_PARAM = "id";
    public static final String IS_REBALANCE_PARAM = "isRebalance";
    public static final String IS_REBALANCE_COLLECTION_PARAM = "isRebalanceCollection";
}
