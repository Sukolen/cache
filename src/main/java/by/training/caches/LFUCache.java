package by.training.caches;

import by.training.ifaces.ICache;

import java.util.*;

/**
 * Class LFUCache
 * @param <K> key with which the specified value is to be associated
 * @param <V> value to be associated with the specified key
 */
public class LFUCache<K, V>  implements ICache<K, V> {
    private Map<K, CacheEntry<V>> hashMap = new HashMap<>();
    private int capacity;

    private final class CacheEntry<T> {
        private int count = 0;
        private T value;

        private CacheEntry(final T value) {
            this.value = value;
        }

        public int getCount() {
            return count;
        }

        public void incCount() {
            count++;
        }
        public T getValue() {
            return value;
        }

    }

    /**
     * Constructor
     * @param capacity size of cache
     */
    public LFUCache(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
    }

    /**
     * Size of cache
     * @return size of cache
     */
    public int size() {
        return hashMap.size();
    }

    /**
     * Returns the value to which the specified key
     * @param key the key whose associated value is to be returned
     * @return the value
     */
    public V get(final Object key) {
        CacheEntry<V> cacheEntry = hashMap.get(key);
        if (cacheEntry == null) {
            return null;
        }
        cacheEntry.incCount();
        return cacheEntry.getValue();
    }

    /**
     * Associates the specified value with the specified key
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return old value
     */
    public V put(final K key, final V value) {
        if (size() == capacity) {
            K minKey = getMinCountKey();
            if (hashMap.containsKey(minKey)) {
                hashMap.remove(minKey);
            }
        }
        CacheEntry<V> cacheEntry = new CacheEntry<>(value);
        cacheEntry = hashMap.put(key, cacheEntry);
        if (cacheEntry != null) {
            return cacheEntry.getValue();
        } else {
            return null;
        }
    }

    /**
     * Removes the value for the specified key
     * @param key key whose value is to be removed from the map
     * @return removed value
     */
    public V remove(final Object key) {
        CacheEntry<V> cacheEntry = hashMap.remove(key);
        if (cacheEntry != null) {
            return cacheEntry.getValue();
        } else {
            return null;
        }
    }

    /**
     * Returns true if this cache contains a value for the specified key.
     * @param key The key whose presence in this cache is to be tested
     * @return true if this cache contains a value for the specified key.
     */
    public boolean containsKey(final Object key) {
        return hashMap.containsKey(key);
    }

    /**
     * Method return all elements collection
     * @return list with all elements
     */
    public List<V> getAll() {
        List<V> list = new ArrayList<>();
        for (Map.Entry<K, CacheEntry<V>> entry : hashMap.entrySet()) {
            list.add(entry.getValue().getValue());
        }
        return list;
    }

    private K getMinCountKey() {
        Map.Entry<K, CacheEntry<V>> min = null;
        for (Map.Entry<K, CacheEntry<V>> entry : hashMap.entrySet()) {
            if (min == null || min.getValue().getCount() > entry.getValue().getCount()) {
                min = entry;
            }
        }
        if (min != null) {
            return min.getKey();
        }
        return null;
    }

    private Map<K, CacheEntry<V>> sort(final Map<K, CacheEntry<V>> oldMap) {
        List<Map.Entry<K, CacheEntry<V>>> list =
                new LinkedList<>(oldMap.entrySet());

        Collections.sort(list, (o1, o2) -> o2.getValue().getCount() - o1.getValue().getCount());

        Map<K, CacheEntry<V>> map = new LinkedHashMap<>();
        for (Map.Entry<K, CacheEntry<V>> entry : list) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    /**
     * Copy cache value to new cache
     * @param cache new cache
     * @return new cache
     */
    public ICache copyCache(final ICache cache) {
        Map<K, CacheEntry<V>> map = sort(hashMap);
        for (Map.Entry<K, CacheEntry<V>> entry : map.entrySet()) {
            cache.put(entry.getKey(), entry.getValue().getValue());
        }
        return cache;
    }
}
