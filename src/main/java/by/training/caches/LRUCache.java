package by.training.caches;

import by.training.ifaces.ICache;

import java.util.*;

/**
 * Class LRUCache
 * @param <K> key with which the specified value is to be associated
 * @param <V> value to be associated with the specified key
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> implements ICache<K, V> {
    private final int capacity;
    private static final float LOAD_FACTOR = 0.75f;

    /**
     * Constructor
     * @param capacity capacity of cache
     */
    public LRUCache(int capacity) {
        super(capacity, LOAD_FACTOR, true);
        this.capacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return size() > capacity;
    }

    /**
     * Method return all elements collection
     * @return list with all elements
     */
    public List<V> getAll() {
        List<V> list = new ArrayList<>();
        for (Map.Entry<K, V> entry : entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    /**
     * Copy cache value to new cache
     * @param cache new cache
     * @return new cache
     */
    public ICache copyCache(final ICache cache) {
        for (Map.Entry<K, V> entry : entrySet()) {
            cache.put(entry.getKey(), entry.getValue());
        }
        return cache;
    }

}
