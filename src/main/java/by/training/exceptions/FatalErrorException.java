package by.training.exceptions;

/**
 * Created by Ruslan on 27.02.2018.
 */
public class FatalErrorException extends Exception {
    public FatalErrorException(String message) {
        super(message);
    }
}
