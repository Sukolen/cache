package by.training.exceptions;

/**
 * Created by Ruslan on 12.12.2017.
 */
public class MergeConflictException extends Exception {
    public MergeConflictException(String message) {
        super(message);
    }
}
