package by.training.exceptions;

/**
 * Created by Ruslan on 12.12.2017.
 */
public class NotFoundException extends Exception {
    public NotFoundException(String message) {
        super(message);
    }
}
