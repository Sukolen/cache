package by.training.exceptions;

/**
 * Created by Flash on 11.01.2018.
 */
public class NodeException extends Exception {
    public NodeException(String message) {
        super(message);
    }
}
