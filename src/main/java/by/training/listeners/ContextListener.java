package by.training.listeners;

import by.training.beans.GroupNode;
import by.training.beans.ObjectValue;
import by.training.beans.ServerNode;
import by.training.constants.Constants;
import by.training.enums.CollectionDAO;
import by.training.enums.ValueDAO;
import by.training.exceptions.DaoException;
import by.training.exceptions.NodeException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import by.training.services.RebalanceService;
import by.training.services.node.NodeCollectionService;
import by.training.services.node.NodeObjectService;
import by.training.services.utils.HealthCheckService;
import by.training.services.utils.HttpService;
import by.training.services.utils.JsonService;
import by.training.services.utils.ThreadExecutorService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ruslan on 12.12.2017.
 */
public class ContextListener implements ServletContextListener {
    private final String executorParamName = "executor";
    private final String schedulerParamName = "scheduler";

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        final String configFile = "config.json";
        final String timeoutParam = "timeout";
        final String updateTimeParam = "updateTime";
        final String rebalanceTimerParam = "rebalanceTimer";
        final String threadPoolCountParam = "threadPoolCount";
        final String serverPortParam = "maven.tomcat.port";
        final String serverIpAddressParam = "ip.address";
        final String serverDefaultPort =  "8080";
        final String serverDefaultIpAddress =  "127.0.0.1";
        final String serverFullAddressParam = "server.address";
        final Logger logger = LogManager.getLogger();
        ServletContext context = servletContextEvent.getServletContext();
        Map<String, ICache<String, ObjectValue>> cacheMap = new HashMap<>();
        ICollectionDAO collectionDAO = CollectionDAO.DB.getCollectionDAO();
        IValueDAO valueDAO = ValueDAO.DB.getValueDAO();
        servletContextEvent.getServletContext().setAttribute(Constants.IS_REBALANCE_PARAM, true);
        servletContextEvent.getServletContext().setAttribute(Constants.IS_REBALANCE_COLLECTION_PARAM, false);
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile);

        JsonObject jsonObject = JsonService.getJsonObjectFromStream(stream);
        try {
            stream.close();
        } catch (IOException e) {
            logger.error(e);
        }

        JsonArray jsonArrayGroups = jsonObject.getAsJsonArray(Constants.GROUPS_PARAM);

        Integer requestTimeout = jsonObject.getAsJsonPrimitive(timeoutParam).getAsInt();

        Integer updateTime = jsonObject.getAsJsonPrimitive(updateTimeParam).getAsInt();

        Integer rebalanceTimer = jsonObject.getAsJsonPrimitive(rebalanceTimerParam).getAsInt();

        Integer threadPoolCount = jsonObject.getAsJsonPrimitive(threadPoolCountParam).getAsInt();

        List<GroupNode> serversGroup = new ArrayList<>();

        ThreadExecutorService executorService = new ThreadExecutorService(threadPoolCount);

        String port = System.getProperty(serverPortParam, serverDefaultPort);

        String ipAddress = System.getProperty(serverIpAddressParam, serverDefaultIpAddress);

        String fullAddress = ipAddress + ":" + port;

        System.setProperty(serverFullAddressParam, fullAddress);

        jsonArrayGroups.forEach((elem) -> {
            JsonArray jsonArray = elem.getAsJsonObject().getAsJsonArray(Constants.SERVERS_PARAM);
            List<ServerNode> servers = new ArrayList<>();
            jsonArray.forEach(jsonElement -> {
                ServerNode serverNode = new ServerNode();
                serverNode.setAddress(jsonElement.getAsString());
                if (fullAddress.equals(jsonElement.getAsString())) {
                    serverNode.setMaster(true);
                    serverNode.setHealth(true);
                }
                servers.add(serverNode);
            });
            GroupNode groupNode = new GroupNode(servers);
            groupNode.setIsMaster(servers.stream().anyMatch(ServerNode::getMaster));
            serversGroup.add(groupNode);

        });
        HealthCheckService healthCheckService =
                new HealthCheckService(new HttpService(requestTimeout), serversGroup, executorService, updateTime);
        NodeObjectService nodeObjectService = new NodeObjectService(cacheMap, collectionDAO, valueDAO,
                serversGroup, executorService, healthCheckService, new HttpService(requestTimeout));
        NodeCollectionService nodeCollectionService = new NodeCollectionService(cacheMap, collectionDAO,
                executorService, healthCheckService, serversGroup, new HttpService(requestTimeout));

        context.setAttribute(Constants.OBJECT_NODE_PARAM, nodeObjectService);
        context.setAttribute(Constants.COLLECTION_NODE_PARAM, nodeCollectionService);
        context.setAttribute(Constants.GROUPS_PARAM, serversGroup);
        context.setAttribute(executorParamName,  executorService);
        RebalanceService rebalanceService = new RebalanceService(collectionDAO, valueDAO,
                executorService, serversGroup, new HttpService(requestTimeout));

        final ScheduledExecutorService scheduler =
                Executors.newScheduledThreadPool(1);

        final Runnable runnable = () -> {
            if (healthCheckService.getDeadServers().size() == 0) {
                try {
                    if (!rebalanceService.getIsRebalanceCollection()) {
                        rebalanceService.rebalanceCollection();
                        servletContextEvent.getServletContext().setAttribute(Constants.IS_REBALANCE_COLLECTION_PARAM, true);
                        logger.info("Rebalancing collections is finished.");
                    }
                    if (healthCheckService.isAllRebalanceCollection()) {
                        rebalanceService.rebalanceObject();
                        scheduler.shutdown();
                        servletContextEvent.getServletContext().setAttribute(Constants.IS_REBALANCE_PARAM, false);
                        logger.info("Rebalancing is finished.");
                    }
                } catch (DaoException | NodeException e) {
                    logger.fatal("Rebalancing is failed. " + e.getMessage());
                    scheduler.shutdown();
                    System.exit(-1);
                }
            }
        };
        scheduler.scheduleWithFixedDelay(runnable, 0, rebalanceTimer, TimeUnit.MILLISECONDS);
        logger.info("Rebalance started.");
        context.setAttribute(schedulerParamName, scheduler);
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ThreadExecutorService executor =
                (ThreadExecutorService) servletContextEvent.getServletContext().getAttribute(executorParamName);
        executor.getExecutor().shutdownNow();
        ScheduledExecutorService scheduler =
                (ScheduledExecutorService) servletContextEvent.getServletContext().getAttribute(schedulerParamName);
        if (!scheduler.isShutdown()) {
            scheduler.shutdownNow();
        }

    }
}
