package by.training.enums;

import by.training.ifaces.ICollectionDAO;
import by.training.impl.DBCollectionImpl;
//import by.training.reflect.DAOInvocationHandler;

import java.lang.reflect.Proxy;

/**
 * Created by Ruslan on 17.12.2017.
 */
public enum CollectionDAO {
    DB {
        @Override
        public ICollectionDAO getCollectionDAO() {
            return new DBCollectionImpl();
        }
    };

    public abstract ICollectionDAO getCollectionDAO();
}
