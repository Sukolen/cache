package by.training.enums;

import by.training.ifaces.IValueDAO;
import by.training.impl.DBValueImpl;
//import by.training.reflect.DAOInvocationHandler;

import java.lang.reflect.Proxy;

/**
 * Created by Ruslan on 17.12.2017.
 */
public enum ValueDAO {
    DB {
        @Override
        public IValueDAO getValueDAO() {
            return new DBValueImpl();
        }
    };
    public abstract IValueDAO getValueDAO();
}
