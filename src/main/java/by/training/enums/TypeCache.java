package by.training.enums;

import by.training.beans.ObjectValue;
import by.training.ifaces.ICache;
import by.training.caches.LFUCache;
import by.training.caches.LRUCache;
//import by.training.reflect.CacheInvocationHandler;

import java.lang.reflect.Proxy;


/**
 * Created by Ruslan on 29.11.2017.
 */
public enum TypeCache {
    LFU {
        @Override
        public ICache<String, ObjectValue> createCache(int capacity) {
            return new LFUCache<>(capacity);
        }
    }, LRU {
        @Override
        public ICache<String, ObjectValue> createCache(int capacity) {
            return new LRUCache<>(capacity);
        }
    };

    abstract ICache<String, ObjectValue> createCache(int capacity);

//    public ICache<String, ObjectValue> getCacheWithProxy(int capacity) {
//        ICache<String, ObjectValue> cache = createCache(capacity);
//        return (ICache<String, ObjectValue>) Proxy.newProxyInstance(cache.getClass().getClassLoader(),
//                cache.getClass().getInterfaces(),
//                new CacheInvocationHandler(cache));
//    }

    public ICache<String, ObjectValue> getCache(int capacity) {
        return createCache(capacity);
    }
}
