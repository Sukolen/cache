package by.training.beans;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class about node group
 */
public class GroupNode {
    private final List<ServerNode> serverNodes;
    private AtomicInteger indexServer = new AtomicInteger(0);
    private Boolean isMaster = false;
    private final int size;

    /**
     * Constructor
     * @param serverNodes list of server
     */
    public GroupNode(final List<ServerNode> serverNodes) {
        this.serverNodes = serverNodes;
        size = serverNodes.size();
    }

    public List<ServerNode> getServerNodes() {
        return serverNodes;
    }


    public Boolean getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    /**
     * Get random server
     * @return server from group
     */
    public ServerNode getNextServer() {
        return serverNodes.get(indexServer.getAndUpdate(operand -> {
            if (operand == size) {
                operand = 0;
            } else {
                operand++;
                if (operand == size) {
                    operand = 0;
                }
            }
            return operand;
        }));
    }
}
