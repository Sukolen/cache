package by.training.beans;


/**
 * Class objectValue
 */
public class ObjectValue {
    private String name;
    private String value;

    /**
     * Constructor
     * @param name name objectValue
     * @param value value (JSON)
     */
    public ObjectValue(final String name, final String value) {
        this.name = name;
        this.value = value;
        validateParam();
    }

    /**
     * Getter for name objectValue
     * @return name objectValue
     */
    public String getName() {
        return name;
    }


    /**
     * Getter for value objectValue
     * @return value (JSON)
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for value
     * @param value value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    private void validateParam() {
        if ((this.name.equals("")) || (this.value.equals(""))) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "ObjectValue{"
                + "name='" + name + '\''
                + ", value='" + value + '\''
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ObjectValue that = (ObjectValue) o;

        if (!name.equals(that.name)) return false;
        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        if (value != null) {
            result = 31 * result + value.hashCode();
        } else {
            result = 31 * result;
        }
        return result;
    }
}
