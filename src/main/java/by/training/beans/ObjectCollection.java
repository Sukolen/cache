package by.training.beans;


import by.training.enums.TypeCache;

/**
 * Class collection
 */
public class ObjectCollection {

    private String name;
    private TypeCache type;
    private int maxCount;
    private String jsonSchema;

    /**
     * Constructor
     * @param name Name collection
     * @param type Type cache
     * @param maxCount Max count elements in cache
     */
    public ObjectCollection(final String name, final String type, int maxCount) {
        this.name = name;
        try {
            this.type = TypeCache.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Type cache is wrong.");
        }
        this.maxCount = maxCount;
        this.jsonSchema = "";
        validateParam(false);
    }

    /**
     * Constructor
     * @param name Name collection
     * @param type Type cache
     * @param maxCount Max count elements in cache
     * @param jsonSchema JSON schema for objects in cache
     */
    public ObjectCollection(final String name, final String type, int maxCount, final String jsonSchema) {
        this.name = name;
        try {
            this.type = TypeCache.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Type cache is wrong.");
        }
        this.maxCount = maxCount;
        this.jsonSchema = jsonSchema;
        validateParam(true);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(TypeCache type) {
        this.type = type;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public void setJsonSchema(String jsonSchema) {
        this.jsonSchema = jsonSchema;
    }

    /**
     * Getter for name
     * @return Name collection
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for type cache
     * @return Type cache
     */
    public TypeCache getType() {
        return type;
    }

    /**
     * Getter for maximum count elements in cache
     * @return  maximum count elements in cache
     */
    public int getMaxCount() {
        return maxCount;
    }

    /**
     * Getter for JSON schema
     * @return JSON schema
     */
    public String getJsonSchema() {
        return jsonSchema;
    }

    private void validateParam(boolean withSchema) {
        String message = "";
        if (this.name.equals("")) {
            message = "Name is empty";
        }
        if ((this.maxCount < 1)) {
            message = "Param 'maxCount' must be more 0";
        }
        if ((withSchema) && (this.jsonSchema.equals(""))) {
            message = "JSON schema is empty";
        }

        if (!message.equals("")) {
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    public String toString() {
        return "ObjectCollection{"
                + "name='" + name + '\''
                + ", type=" + type
                + ", maxCount=" + maxCount
                + ", jsonSchema='" + jsonSchema + '\''
                + '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObjectCollection that = (ObjectCollection) o;

        return maxCount == that.maxCount && name.equals(that.name)
                && type == that.type && jsonSchema.equals(that.jsonSchema);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + maxCount;
        return result;
    }
}
