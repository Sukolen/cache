package by.training.beans;

import by.training.ifaces.ICache;
import by.training.caches.LFUCache;
import by.training.caches.LRUCache;
//import by.training.reflect.CacheInvocationHandler;

import java.lang.reflect.Proxy;


/**
 * Created by Ruslan on 29.11.2017.
 */
public enum TypeCache {
    LFU {
        @Override
        public ICache<String, ObjectValue> createCache(int capacity) {
            return new LFUCache<>(capacity);
        }
    }, LRU {
        @Override
        public ICache<String, ObjectValue> createCache(int capacity) {
            return new LRUCache<>(capacity);
        }
    };

    abstract ICache<String, ObjectValue> createCache(int capacity);


    public ICache<String, ObjectValue> getCache(int capacity) {
        return createCache(capacity);
    }
}
