package by.training.beans;

import java.time.LocalDateTime;

/**
 * Created by Ruslan on 09.01.2018.
 */
public class ServerNode {
    private String address;
    private Boolean master = false;
    private Boolean health = false;
    private Boolean isRebalanceCollection = false;
    private LocalDateTime dateTime = LocalDateTime.now();

    public ServerNode() {
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Boolean getHealth() {
        return health;
    }

    public void setHealth(Boolean health) {
        this.health = health;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getMaster() {
        return master;
    }

    public void setMaster(Boolean master) {
        this.master = master;
    }

    public Boolean getIsRebalanceCollection() {
        return isRebalanceCollection;
    }

    public void setIsRebalanceCollection(Boolean isRebalanceCollection) {
        this.isRebalanceCollection = isRebalanceCollection;
    }
}
