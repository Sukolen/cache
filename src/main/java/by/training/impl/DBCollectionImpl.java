package by.training.impl;

import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.exceptions.MergeConflictException;
import by.training.ifaces.ICollectionDAO;
import by.training.beans.ObjectCollection;
import by.training.enums.TypeCache;
//import by.training.reflect.AclLog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 29.11.2017.
 */
public class DBCollectionImpl extends AbstractBaseDB
        implements ICollectionDAO {
    private final int firstIdx = 1;
    private final int secondIdx = 2;
    private final int thirdsIdx = 3;
    private final int fourthIdx = 4;
    private final String dbSqlError = "SQL error.";
    private final Object lock = new Object();

    /**
     * Create collection in DB
     * @param collection object collection who should be save in DB
     * @throws DaoException DAO exception
     */
    public final void create(final ObjectCollection collection) throws DaoException {
        final String insertSql = "INSERT INTO COLLECTIONS(NAMECOLLECTION, TYPECACHE, MAXCOUNT, JSONSCHEMA)"
                               + " VALUES(?, ?, ?, ?)";
        final String checkSql = "SELECT NAMECOLLECTION FROM COLLECTIONS WHERE NAMECOLLECTION = ?";
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(insertSql);
             PreparedStatement stmtIsCollection = connection.prepareStatement(checkSql)) {
            stmt.setString(firstIdx, collection.getName());
            stmt.setString(secondIdx, collection.getType().name());
            stmt.setInt(thirdsIdx, collection.getMaxCount());
            stmt.setString(fourthIdx, collection.getJsonSchema());
            synchronized (lock) {
                stmtIsCollection.setString(firstIdx, collection.getName());
                try (ResultSet resultSet = stmtIsCollection.executeQuery()) {
                    if (resultSet.next()) {
                        final String alreadyCreatedMsg = "Collection already created.";
                        throw new DaoException(alreadyCreatedMsg);
                    }
                }
                stmt.execute();
            }
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
    }

    /**
     * Read collection from DB
     * @param name name collection
     * @return object collection
     * @throws DaoException DAO exception
     */
    public ObjectCollection read(final String name) throws DaoException {
        final String selectSql = "SELECT nameCollection, typeCache, maxCount, jsonSchema"
                               + " FROM collections WHERE nameCollection = ?";
        ObjectCollection objectCollection = null;

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(selectSql)) {
            stmt.setString(firstIdx, name);
            try (ResultSet rs =  stmt.executeQuery()) {
                while (rs.next()) {
                    objectCollection = new ObjectCollection(name,
                            rs.getString(secondIdx),
                            rs.getInt(thirdsIdx),
                            rs.getString(fourthIdx));
                }
            }
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
        return objectCollection;
    }

    public List<ObjectCollection> read(final String count, final String id) throws DaoException {
        String selectSql = "SELECT TOP ? nameCollection, typeCache, maxCount, jsonSchema FROM collections ";
        if (!Constants.EMPTY.equals(id)) {
            selectSql = selectSql + "WHERE nameCollection > ? ";
        }
        selectSql = selectSql + "ORDER BY nameCollection";
        List<ObjectCollection> objectCollections = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(selectSql)) {
            stmt.setString(firstIdx, count);
            if (!Constants.EMPTY.equals(id)) {
                stmt.setString(secondIdx, id);
            }
            try (ResultSet rs =  stmt.executeQuery()) {
                while (rs.next()) {
                    String nameCollection = rs.getString(firstIdx);
                    objectCollections.add(new ObjectCollection(nameCollection,
                            rs.getString(secondIdx),
                            rs.getInt(thirdsIdx),
                            rs.getString(fourthIdx)));
                }
            }
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
        return objectCollections;

    }

    /**
     * Replace collection in DB
     * @param name name collection
     * @param newName new name collection
     * @param typeCache new type cache
     * @param maxCount new maxCount
     * @throws DaoException DAO exception
     */
    public void update(final String name, final String newName,
                       final TypeCache typeCache, int maxCount) throws DaoException {
        final String updateSql = "UPDATE collections"
                               + " SET nameCollection = ?, typeCache = ?, maxCount = ? WHERE nameCollection = ?";

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(updateSql)) {
            stmt.setString(firstIdx, newName);
            stmt.setString(secondIdx, typeCache.name());
            stmt.setInt(thirdsIdx, maxCount);
            stmt.setString(fourthIdx, name);
            stmt.execute();
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
    }

    /**
     * Remove collection from DB
     * @param name name collection
     * @throws DaoException DAO exception
     */
    public void delete(final String name) throws DaoException {
        final String deleteSql = "DELETE FROM collections WHERE NAMECOLLECTION = ?";

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(deleteSql)) {
            stmt.setString(firstIdx, name);
            stmt.execute();
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
    }
}
