package by.training.impl;



import by.training.exceptions.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;

/**
 * Created by Ruslan on 10.08.2017.
 */
public abstract class AbstractBaseDB {
    static final Logger LOGGER1 = LogManager.getLogger();

    protected Connection getConnection() throws DaoException {
        Context envCtx;
        DataSource ds;
        try {
        envCtx = (Context) (new InitialContext().lookup("java:comp/env"));
        ds = (DataSource) envCtx.lookup(System.getProperty("server.address"));
            return ds.getConnection();
        } catch (SQLException | NamingException e) {
            String errorResource = "Problems with resource.";
            throw new DaoException(errorResource + ": " + e.getMessage());
        }
    }
}
