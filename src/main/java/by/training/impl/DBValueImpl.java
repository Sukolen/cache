package by.training.impl;

import by.training.constants.Constants;
import by.training.exceptions.DaoException;
import by.training.ifaces.IValueDAO;
import by.training.beans.ObjectValue;
//import by.training.reflect.AclLog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 29.11.2017.
 */
public class DBValueImpl extends AbstractBaseDB implements IValueDAO {
    private final int firstIdx = 1;
    private final int secondIdx = 2;
    private final int thirdsIdx = 3;
    private final String dbSqlError = "SQL error.";
    private final Object lock = new Object();



    /**
     * Create objectValue in DB
     * @param collectionName name collection
     * @param objectValue objectValue
     * @throws DaoException DAO exception
     */
    public void create(final String collectionName, final ObjectValue objectValue) throws DaoException {
        final String insertSql = "INSERT INTO objects (IDCOLLECTIONS, NAMEOBJECT, VALUEOBJECT)"
                + " VALUES(?, ?, ?)";
        final String checkSql = "SELECT NAMEOBJECT FROM OBJECTS WHERE IDCOLLECTIONS = ? AND NAMEOBJECT = ?";

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(insertSql);
             PreparedStatement stmtIsObject = connection.prepareStatement(checkSql);) {
            stmt.setString(firstIdx, collectionName);
            stmt.setString(secondIdx, objectValue.getName());
            stmt.setString(thirdsIdx, objectValue.getValue());
            synchronized (lock) {
                stmtIsObject.setString(firstIdx, collectionName);
                stmtIsObject.setString(secondIdx, objectValue.getName());
                try (ResultSet resultSet = stmtIsObject.executeQuery()) {
                    if (resultSet.next()) {
                        final String alreadyCreatedMsg = "Object already created.";
                        throw new DaoException(alreadyCreatedMsg);
                    }
                }
                stmt.execute();
            }
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
    }

    /**
     * Read objectValue from DB
     * @param collection name collection
     * @param name name objectValue
     * @return objectValue
     * @throws DaoException DAO exception
     */
    public ObjectValue read(final String collection, final String name) throws DaoException {
        final String selectSql = "SELECT IDCOLLECTIONS, nameObject, valueObject"
                + " FROM objects WHERE IDCOLLECTIONS = ? AND nameObject = ?";
        ObjectValue objectValue = null;
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(selectSql)) {
            stmt.setString(firstIdx, collection);
            stmt.setString(secondIdx, name);
            try (ResultSet rs =  stmt.executeQuery()) {
                while (rs.next()) {
                    objectValue = new ObjectValue(name, rs.getString(thirdsIdx));
                }
            }
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
        return objectValue;
    }


    public List<ObjectValue> read(final String collection, final String count, final String id) throws DaoException {
        String selectSql = "SELECT TOP ? nameObject, valueObject FROM objects WHERE IDCOLLECTIONS = ? ";
        if (!Constants.EMPTY.equals(id)) {
            selectSql = selectSql + "AND nameObject > ? ";
        }
        selectSql = selectSql + "ORDER BY nameObject";
        List<ObjectValue> objectValues = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(selectSql)) {
            stmt.setString(firstIdx, count);
            stmt.setString(secondIdx, collection);
            if (!Constants.EMPTY.equals(id)) {
                stmt.setString(thirdsIdx, id);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    objectValues.add(new ObjectValue(rs.getString(firstIdx), rs.getString(secondIdx)));
                }
            }
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
        return objectValues;

    }

    /**
     * Replace objectValue in DB
     * @param collectionName name collection
     * @param objectValue new objectValue
     * @throws DaoException DAO exception
     */
    public void update(final String collectionName, final ObjectValue objectValue) throws DaoException {
        final String updateSql = "UPDATE objects"
                + " SET valueObject = ? WHERE IDCOLLECTIONS = ? AND nameObject = ?";

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(updateSql)) {
            stmt.setString(firstIdx, objectValue.getValue());
            stmt.setString(secondIdx, collectionName);
            stmt.setString(thirdsIdx, objectValue.getName());
            stmt.execute();
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
    }

    /**
     * Remove objectValue from DB
     * @param collectionName name collection
     * @param valueName name objectValue
     * @throws DaoException DAO exception
     */
    public void delete(final String collectionName, final String valueName) throws DaoException {
        final String deleteSql = "DELETE FROM objects WHERE IDCOLLECTIONS = ? AND nameObject = ?";

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(deleteSql)) {
            stmt.setString(firstIdx, collectionName);
            stmt.setString(secondIdx, valueName);
            stmt.execute();
        } catch (SQLException e) {
            LOGGER1.error(dbSqlError + e.getMessage());
            throw new DaoException(dbSqlError);
        }
    }
}
