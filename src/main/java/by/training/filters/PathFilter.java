package by.training.filters;

import by.training.constants.Constants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Flash on 05.12.2017.
 */
public class PathFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Boolean isRebalance = (Boolean) request.getServletContext().getAttribute(Constants.IS_REBALANCE_PARAM);
        if (isRebalance) {
            HttpServletResponse res = (HttpServletResponse) response;
            res.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            return;
        }
        String pathInfo = ((HttpServletRequest) request).getPathInfo();
        if (pathInfo == null) {
            request.setAttribute(Constants.SERVLET_PARAM_COLLECTION, "");
            request.setAttribute(Constants.SERVLET_PARAM_OBJECT, "");
        } else {
            pathInfo =  pathInfo.replaceFirst("/", "");
            String[] pathParts = pathInfo.split("/");
            switch (pathParts.length) {
                case 0:
                    request.setAttribute(Constants.SERVLET_PARAM_COLLECTION, "");
                    request.setAttribute(Constants.SERVLET_PARAM_OBJECT, "");
                    break;
                case 1:
                    request.setAttribute(Constants.SERVLET_PARAM_COLLECTION, pathParts[0].trim());
                    request.setAttribute(Constants.SERVLET_PARAM_OBJECT, "");
                    break;
                default:
                    request.setAttribute(Constants.SERVLET_PARAM_COLLECTION, pathParts[0].trim());
                    request.setAttribute(Constants.SERVLET_PARAM_OBJECT, pathParts[1].trim());
                    break;
            }
        }
        if (request.getParameter(Constants.COUNT_PARAM) == null) {
            request.setAttribute(Constants.COUNT_PARAM, "10");
        } else {
            request.setAttribute(Constants.COUNT_PARAM, request.getParameter(Constants.COUNT_PARAM));
        }
        if (request.getParameter(Constants.ID_PARAM) == null) {
            request.setAttribute(Constants.ID_PARAM, Constants.EMPTY);
        } else {
            request.setAttribute(Constants.ID_PARAM, request.getParameter(Constants.ID_PARAM));
        }

        chain.doFilter(request, response);
    }


    public void destroy() {

    }
}
