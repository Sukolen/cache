package by.training.caches;

import by.training.ifaces.ICache;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Flash on 29.11.2017.
 */
public class MyLRUCacheTest {
    private ICache<String, String> cache;
    @Test
    public void testRemoveEldestEntry() throws Exception {
        cache = new LRUCache<>(3);
        cache.put("cat", "Barsik");
        cache.put("dog", "Bim");
        cache.put("hamster", "Ted");
        cache.put("cow", "Burenka");
        assertEquals(null, cache.get("cat"));
        assertEquals(3, cache.size());

    }
    @Test
    public void testGetAll() throws Exception {
        cache = new LRUCache<>(10);
        cache.put("cat", "Barsik");
        cache.put("dog", "Bim");
        List list = cache.getAll();
        assertEquals(list, Arrays.asList("Barsik", "Bim"));
    }

    @Test
    public void testCopyCache() throws Exception {
        cache = new LRUCache<>(2);
        cache.put("dog", "Bim");
        cache.put("hamster", "Ted");
        cache.get("dog");
        cache.put("cat", "Barsik");
        ICache<String, String> newCache = new LFUCache<>(2);
        cache.copyCache(newCache);
        List<String> list = newCache.getAll();

        assertTrue(list.containsAll(Arrays.asList("Bim", "Barsik")));
    }

}