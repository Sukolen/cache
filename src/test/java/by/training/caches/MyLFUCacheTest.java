package by.training.caches;

import by.training.ifaces.ICache;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ruslan on 28.11.2017.
 */
public class MyLFUCacheTest {
    private ICache<String, String> cache;

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNonPositive() {
        cache = new LFUCache<>(-1);
    }

    @Test
    public void testConstructor() {
        cache = new LFUCache<>(0);
        cache.put("cat", "Barsik");
    }


    @Test
    public void testSize() throws Exception {
        cache = new LFUCache<>(2);
        assertEquals(0, cache.size());
        cache.put("cat", "Barsik");
        cache.get("cat");
        assertEquals(1, cache.size());
        cache.put("dog", "Bim");
        assertEquals(2, cache.size());
        cache.put("hamster", "Ted");
        assertEquals(2, cache.size());
        cache.put("cow", "Burenka");
        assertEquals(2, cache.size());
    }

    @Test
    public void testGet() throws Exception {
        cache = new LFUCache<>(10);
        cache.put("cat", "Barsik");
        cache.put("dog", "Bim");
        String actual = cache.get("cat");
        assertEquals("Barsik", actual);
    }

    @Test
    public void testGetNotFoundKey() throws Exception {
        cache = new LFUCache<>(10);
        String actual =  cache.get("Not");
        assertEquals(actual, null);
    }

    @Test
    public void testPut() throws Exception {
        cache = new LFUCache<>(10);
        cache.put("cat", "Barsik");
        cache.put("dog", "Bim");
        assertEquals(true, cache.containsKey("dog"));
    }

    @Test
    public void testPutPrevValue() throws Exception {
        cache = new LFUCache<>(10);
        cache.put("cat", "Barsik");
        String prev = cache.put("cat", "Bim");
        assertEquals("Barsik", prev);
    }


    @Test
    public void testRemove() throws Exception {
        cache = new LFUCache<>(10);
        cache.put("cat", "Barsik");
        cache.put("dog", "Bim");
        cache.remove("dog");
        assertEquals(false, cache.containsKey("dog"));
    }

    @Test
    public void testRemoveNotFoundElement() throws Exception {
        cache = new LFUCache<>(10);
        String result = cache.remove("dog");
        assertNull(result);
    }
    @Test
    public void testContainsKey() throws Exception {
        cache = new LFUCache<>(10);
        cache.put("cat", "Barsik");
        assertEquals(false, cache.containsKey("dog"));
        assertEquals(true, cache.containsKey("cat"));
    }

    @Test
    public void testCache() throws Exception {
        cache = new LFUCache<>(2);
        cache.put("cat", "Barsik");
        cache.get("cat");
        cache.get("cat");
        cache.put("dog", "Bim");
        cache.get("dog");
        cache.put("hamster", "Ted");
        assertEquals(false, cache.containsKey("dog"));
    }

    @Test
    public void testGetAll() throws Exception {
        cache = new LFUCache<>(10);
        cache.put("cat", "Barsik");
        cache.put("dog", "Bim");
        List list = cache.getAll();
        assertEquals(list, Arrays.asList("Barsik", "Bim"));
    }

    @Test
    public void testCopyCacheAndSort() throws Exception {
        cache = new LFUCache<>(5);
        cache.put("dog", "Bim");
        cache.get("dog");
        cache.put("hamster", "Ted");
        cache.put("cat", "Barsik");
        cache.get("cat");
        cache.get("cat");
        ICache<String, String> newCache = new LRUCache<>(5);

        List<String> sortedList;
        cache.copyCache(newCache);
        sortedList = newCache.getAll();
        assertTrue(sortedList.containsAll(Arrays.asList("Barsik", "Bim", "Ted")));


    }
}