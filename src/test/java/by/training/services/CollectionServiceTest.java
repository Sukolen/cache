package by.training.services;

import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.caches.LFUCache;
import by.training.exceptions.MergeConflictException;
import by.training.exceptions.NotFoundException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Ruslan on 23.01.2018.
 */
public class CollectionServiceTest {
    private Map<String, ICache<String, ObjectValue>> cacheMap;
    private ObjectCollection objectCollection;
    private ObjectValue barsik;
    private ICollectionDAO collectionDAO;
    private CollectionService collectionService;
    private ObjectService objectService;

    @Before
    public void testBefore() throws Exception {
        cacheMap = new HashMap<>();
        objectCollection = new ObjectCollection("Cats", "LFU", 10, "{}");
        cacheMap.put(objectCollection.getName(), objectCollection.getType().getCache(objectCollection.getMaxCount()));
        barsik = new ObjectValue("Barsik", "{}");

        IValueDAO valueDAO = mock(IValueDAO.class);
        collectionDAO = mock(ICollectionDAO.class);
        collectionService = new CollectionService(cacheMap, collectionDAO);
        objectService = new ObjectService(cacheMap, collectionDAO, valueDAO);
    }


    @Test
    public void testCreateAndDeleteCollection() throws Exception {
        collectionService.deleteCollection(objectCollection.getName());

        assertEquals(false, cacheMap.containsKey(objectCollection.getName()));

        collectionService.createCollection(objectCollection);
        assertEquals(true, cacheMap.containsKey(objectCollection.getName()));
    }

    @Test(expected = NotFoundException.class)
    public void testReadCollectionIsNotInDB() throws Exception {
        collectionService.readCollection("Dogs");
    }


    @Test
    public void testReadCollection() throws Exception {
        when(collectionDAO.read(objectCollection.getName())).thenReturn(objectCollection);
        assertEquals(collectionService.readCollection(objectCollection.getName()), objectCollection);
    }

    @Test
    public void testReadCollections() throws Exception {
        List<ObjectCollection> objectCollections = new ArrayList<>();
        objectCollections.add(objectCollection);
        objectCollections.add(new ObjectCollection("dogs", "LFU", 10));

        when(collectionDAO.read(anyString(), anyString())).thenReturn(objectCollections);
        assertEquals(collectionService.readCollection("10", "a"), objectCollections);
    }


    @Test
    public void testUpdateAndReplaceCacheCollection() throws Exception {
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        ObjectCollection dogsCollection = new ObjectCollection("Dogs", "LRU", 20, "{}");
        objectService.createObject("Cats", barsik);
        objectService.createObject("Cats", new ObjectValue("kuzya", "{}"));

        collectionService.updateCollection("Cats", dogsCollection);
        assertEquals(true, cacheMap.containsKey("Dogs"));

        cacheMap.remove("Dogs");
        collectionService.updateCollection("Cats", dogsCollection);
        assertEquals(true, cacheMap.containsKey("Dogs"));
    }


    @Test
    public void testUpdateOrCreateCollection() throws Exception {
        ObjectCollection dogsCollection = new ObjectCollection("Dogs", "LRU", 20, "{}");
        collectionService.updateOrCreateCollection(dogsCollection);
        assertEquals(cacheMap.containsKey(dogsCollection.getName()), true);
    }

    @Test(expected = MergeConflictException.class)
    public void testUpdateOrCreateCollectionMergeConflict() throws Exception {
        ObjectCollection cats2 = new ObjectCollection("Cats", "LRU", 20, "{}");
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        collectionService.updateOrCreateCollection(cats2);
    }


}