package by.training.services.node;

import by.training.beans.GroupNode;
import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.beans.ServerNode;
import by.training.exceptions.FatalErrorException;
import by.training.exceptions.NodeException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.services.utils.HealthCheckService;
import by.training.services.utils.HttpService;
import by.training.services.utils.ThreadExecutorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Flash on 24.01.2018.
 */
public class NodeCollectionServiceTest {
    private Map<String, ICache<String, ObjectValue>> cacheMap;
    private ICollectionDAO collectionDAO;
    private HttpService httpService;
    private ThreadExecutorService executorService;
    private ObjectCollection objectCollection;
    private NodeCollectionService collectionService;
    private ServerNode serverNode2 = new ServerNode();
    private ServerNode serverNode3 = new ServerNode();
    private Map<String, Object> respOK = new HashMap<>();
    private Map<String, Object> respCreate = new HashMap<>();
    private Map<String, Object> respBad = new HashMap<>();



    @Before
    public void setUp() throws Exception {
        cacheMap = new HashMap<>();
        List<ServerNode> servers = new ArrayList<>();
        List<ServerNode> servers2 = new ArrayList<>();
        List<GroupNode> groupNodes = new ArrayList<>();
        collectionDAO = mock(ICollectionDAO.class);
        httpService = mock(HttpService.class);
        executorService = new ThreadExecutorService(10);
        ServerNode serverNode = new ServerNode();
        serverNode.setAddress("127.0.0.1:8080");
        serverNode.setMaster(true);
        serverNode.setHealth(true);
        servers.add(serverNode);
        GroupNode groupNode = new GroupNode(servers);
        serverNode2.setAddress("127.0.0.1:8181");
        serverNode2.setHealth(true);
        servers2.add(serverNode2);
        serverNode3.setAddress("127.0.0.1:8282");
        serverNode3.setHealth(true);
        servers2.add(serverNode3);
        GroupNode groupNode2 = new GroupNode(servers2);
        groupNodes.add(groupNode);
        groupNodes.add(groupNode2);

        respOK.put("statusCode", 200);
        respCreate.put("statusCode", 201);
        respBad.put("statusCode", 400);


        HealthCheckService checkService = new HealthCheckService(httpService, groupNodes, executorService, 5000);
        objectCollection = new ObjectCollection("Cats", "LFU", 10, "{}");
        collectionService =
                new NodeCollectionService(cacheMap, collectionDAO,
                        executorService, checkService, groupNodes,  httpService);
    }

    @After
    public void tearDown() throws Exception {
        executorService.getExecutor().shutdownNow();
    }

    @Test
    public void testDeleteCollection() throws Exception {
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respOK);
        collectionService.createCollection(objectCollection, false, "", "");
        assertEquals(true, cacheMap.containsKey(objectCollection.getName()));
        collectionService.deleteCollection("Cats", false, "");
        assertEquals(false, cacheMap.containsKey(objectCollection.getName()));
    }

    @Test
    public void testDeleteCollectionOnNotMasterNode() throws Exception {
        collectionService.createCollection(objectCollection, true, "", "");
        assertEquals(true, cacheMap.containsKey(objectCollection.getName()));
        collectionService.deleteCollection("Cats", true, "");
        assertEquals(false, cacheMap.containsKey(objectCollection.getName()));
    }

    @Test(expected = NodeException.class)
    public void testDeleteCollectionHealthCheckIsFalse() throws Exception {
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        serverNode2.setHealth(false);
        collectionService.deleteCollection("Cats", false, "");
    }

    @Test(expected = NodeException.class)
    public void testDeleteCollectionWithWrongRequest() throws Exception {
        when(httpService.sendDelete(eq(serverNode2.getAddress()), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(eq(serverNode3.getAddress()), anyString())).thenAnswer(invocationOnMock -> {
            Thread.sleep(2000);
            return respBad;
        });
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        collectionService.deleteCollection("Cats", false, "");
    }

    @Test
    public void testCreateCollection() throws Exception {
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        collectionService.createCollection(objectCollection, false, "", "");
        assertEquals(true, cacheMap.containsKey(objectCollection.getName()));
    }

    @Test(expected = NodeException.class)
    public void testCreateCollectionExecutionException() throws Exception {
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenAnswer(invocationOnMock -> NodeException.class);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respOK);
        collectionService.createCollection(objectCollection, false, "", "");
    }

    @Test(expected = FatalErrorException.class)
    public void testCreateCollectionWithWrongRequest() throws Exception {

        when(httpService.sendPost(eq(serverNode2.getAddress()), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendPost(eq(serverNode3.getAddress()), anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respBad);

        collectionService.createCollection(objectCollection, false, "", "");
    }

    @Test(expected = NodeException.class)
    public void testCreateCollectionWithWrongRequest2() throws Exception {

        when(httpService.sendPost(eq(serverNode2.getAddress()), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendPost(eq(serverNode3.getAddress()), anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respOK);

        collectionService.createCollection(objectCollection, false, "", "");
    }

    @Test
    public void testCreateCollectionOnNotMasterNode() throws Exception {
        collectionService.createCollection(objectCollection, true, "", "");
        assertEquals(true, cacheMap.containsKey(objectCollection.getName()));
    }

    @Test(expected = NodeException.class)
    public void testCreateCollectionHealthCheckIsFalse() throws Exception {
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        serverNode2.setHealth(false);
        collectionService.createCollection(objectCollection, false, "", "");
    }


    @Test
    public void testUpdateCollection() throws Exception {
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        collectionService.createCollection(objectCollection, false, "", "");
        assertEquals(true, cacheMap.containsKey(objectCollection.getName()));

        ObjectCollection newCollection = new ObjectCollection("Dogs", "LRU", 10, "{}");
        when(collectionDAO.read(objectCollection.getName())).thenReturn(objectCollection);
        collectionService.updateCollection("Cats", newCollection, false, "", "");
        assertEquals(true, cacheMap.containsKey(newCollection.getName()));
    }

    @Test(expected = FatalErrorException.class)
    public void testUpdateCollectionWithWrongRequest() throws Exception {
        when(httpService.sendPut(eq(serverNode2.getAddress()), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq(serverNode3.getAddress()), anyString(), anyString())).thenReturn(respBad);
        ObjectCollection newCollection = new ObjectCollection("Dogs", "LRU", 10, "{}");
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq(serverNode2.getAddress()), eq("/app/collections/" + newCollection.getName()), anyString())).thenReturn(respBad);
        when(collectionDAO.read(objectCollection.getName())).thenReturn(objectCollection);
        collectionService.updateCollection("Cats", newCollection, false, "", "");
    }

    @Test(expected = NodeException.class)
    public void testUpdateCollectionWithWrongRequest2() throws Exception {
        when(httpService.sendPut(eq(serverNode2.getAddress()), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq(serverNode3.getAddress()), anyString(), anyString())).thenReturn(respBad);
        ObjectCollection newCollection = new ObjectCollection("Dogs", "LRU", 10, "{}");
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(collectionDAO.read(objectCollection.getName())).thenReturn(objectCollection);
        collectionService.updateCollection("Cats", newCollection, false, "", "");
    }

    @Test
    public void testUpdateCollectionOnNotMasterNode() throws Exception {
        when(collectionDAO.read(objectCollection.getName())).thenReturn(objectCollection);
        ObjectCollection newCollection = new ObjectCollection("Dogs", "LRU", 10, "{}");
        collectionService.updateCollection("Cats", newCollection, true, "", "");
        assertEquals(true, cacheMap.containsKey(newCollection.getName()));
    }

    @Test(expected = NodeException.class)
    public void testUpdateCollectionHealthCheckIsFalse() throws Exception {
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        serverNode2.setHealth(false);
        collectionService.updateCollection("Cats", objectCollection, false, "", "");
    }





}