package by.training.services.node;

import by.training.beans.GroupNode;
import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.beans.ServerNode;
import by.training.constants.Constants;
import by.training.exceptions.FatalErrorException;
import by.training.exceptions.NodeException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import by.training.services.utils.HealthCheckService;
import by.training.services.utils.HttpService;
import by.training.services.utils.JsonService;
import by.training.services.utils.ThreadExecutorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalMatchers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Flash on 24.01.2018.
 */
public class NodeObjectServiceTest {
    private Map<String, ICache<String, ObjectValue>> cacheMap;
    private IValueDAO valueDAO;
    private HttpService httpService;
    private ThreadExecutorService executorService;
    private ObjectCollection objectCollection;
    private ObjectValue baronCat;
    private NodeObjectService objectService;
    private ServerNode serverNode = new ServerNode();
    private ServerNode serverNode2 = new ServerNode();
    private ServerNode serverNode3 = new ServerNode();
    private ServerNode serverNode4 = new ServerNode();
    private Map<String, Object> respOK = new HashMap<>();
    private Map<String, Object> respCreate = new HashMap<>();
    private Map<String, Object> respBad = new HashMap<>();




    @Before
    public void setUp() throws Exception {
        cacheMap = new HashMap<>();
        List<ServerNode> servers = new ArrayList<>();
        List<ServerNode> servers2 = new ArrayList<>();

        List<GroupNode> groupServers = new ArrayList<>();
        ICollectionDAO collectionDAO = mock(ICollectionDAO.class);
        valueDAO = mock(IValueDAO.class);
        httpService = mock(HttpService.class);
        executorService = new ThreadExecutorService(10);

        serverNode.setAddress("127.0.0.1:8080");
        serverNode.setMaster(true);
        serverNode.setHealth(true);
        servers.add(serverNode);
        serverNode4.setAddress("127.0.0.1:8484");
        serverNode4.setHealth(true);
        servers.add(serverNode4);

        GroupNode groupNode = new GroupNode(servers);
        groupNode.setIsMaster(true);
        groupServers.add(groupNode);
        serverNode2.setAddress("127.0.0.1:8181");
        serverNode2.setHealth(true);
        servers2.add(serverNode2);
        serverNode3.setAddress("127.0.0.1:8282");
        serverNode3.setHealth(true);
        servers2.add(serverNode3);

        GroupNode groupNode2 = new GroupNode(servers2);
        groupServers.add(groupNode2);

        respOK.put("statusCode", 200);
        respCreate.put("statusCode", 201);
        respBad.put("statusCode", 400);
        HealthCheckService checkService = new HealthCheckService(httpService, groupServers, executorService, 5000);
        objectCollection = new ObjectCollection("Cats", "LFU", 10, "{}");
        cacheMap.put(objectCollection.getName(), objectCollection.getType().getCache(objectCollection.getMaxCount()));
        baronCat = new ObjectValue("Baron", "{}");
        objectService =
                new NodeObjectService(cacheMap, collectionDAO,
                    valueDAO, groupServers, executorService, checkService, httpService);

        when(collectionDAO.read(objectCollection.getName())).thenReturn(objectCollection);
    }

    @After
    public void tearDown() throws Exception {
        executorService.getExecutor().shutdownNow();
    }

    @Test
    public void testCreateObjectOnMaster() throws Exception {
        objectService.createObject(objectCollection.getName(), baronCat,  "", true);
        assertEquals(cacheMap.get(objectCollection.getName()).get(baronCat.getName()), baronCat);
    }

    @Test
    public void testCreateObjectOnAnotherServer() throws Exception {
        ObjectValue barsikCat = new ObjectValue("Barsik", "{}");
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        objectService.createObject(objectCollection.getName(), barsikCat,  "", false);
    }

    @Test(expected = FatalErrorException.class)
    public void testCreateObjectOnAnotherServerWithWrongRequest() throws Exception {
        serverNode.setMaster(false);

        when(httpService.sendPost(eq("127.0.0.1:8080"), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendPost(eq("127.0.0.1:8484"), anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respBad);
        objectService.createObject(objectCollection.getName(), baronCat,  "", false);
    }
    @Test(expected = NodeException.class)
    public void testCreateObjectOnAnotherServerWithWrongRequest2() throws Exception {
        serverNode.setMaster(false);

        when(httpService.sendPost(eq("127.0.0.1:8080"), anyString(), anyString())).thenReturn(respCreate);
        when(httpService.sendPost(eq("127.0.0.1:8484"), anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respOK);
        objectService.createObject(objectCollection.getName(), baronCat,  "", false);
    }


    @Test(expected = NodeException.class)
    public void testCreateObjectExecutionException() throws Exception {
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenAnswer(invocationOnMock -> NodeException.class);

        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respOK);
        objectService.createObject(objectCollection.getName(), baronCat,  "", false);
    }



    @Test
    public void testReadObjectFromMaster() throws Exception {
        objectService.createObject(objectCollection.getName(), baronCat);
        assertEquals(objectService.readObject(objectCollection.getName(), baronCat.getName(), "", false), baronCat);
    }

    @Test(expected = NodeException.class)
    public void testReadObjectFromDeadServers() throws Exception {
        serverNode.setMaster(true);
        serverNode.setHealth(false);
        serverNode4.setHealth(false);

        objectService.readObject(objectCollection.getName(), baronCat.getName(), "", false);
    }


    @Test(expected = NodeException.class)
    public void testReadObjectFromAnotherServerWithWrongRequest() throws Exception {
        serverNode.setMaster(false);
        ObjectValue barsikCat = new ObjectValue("Barsik", "{}");

        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        objectService.readObject(objectCollection.getName(), barsikCat.getName(),  "", false);
    }

    @Test
    public void testReadObjectFromAnotherServer() throws Exception {
        serverNode.setMaster(false);
        ObjectValue barsikCat = new ObjectValue("Barsik", "{}");

        Map<String, Object> respGet = new HashMap<>();
        respGet.put("statusCode", 200);
        respGet.put(Constants.RESPONSE_BODY, JsonService.getJSON(barsikCat));
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respGet);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        assertEquals(objectService.readObject(objectCollection.getName(), barsikCat.getName(), "", false), barsikCat);

    }

    @Test
    public void testDeleteObjectFromMaster() throws Exception {
        objectService.createObject(objectCollection.getName(), baronCat, "", true);
        assertEquals(cacheMap.get(objectCollection.getName()).get(baronCat.getName()), baronCat);
        objectService.deleteObject(objectCollection.getName(), baronCat.getName(), "", true);
        assertEquals(cacheMap.get(objectCollection.getName()).get(baronCat.getName()), null);
    }

    @Test
    public void testDeleteObjectFromAnotherServer() throws Exception {
        serverNode.setMaster(true);
        ObjectValue barsikCat = new ObjectValue("Barsik", "{}");
        Map<String, Object> respDelete = new HashMap<>();
        respDelete.put("statusCode", 200);
        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respDelete);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respDelete);
        objectService.deleteObject(objectCollection.getName(), barsikCat.getName(), "", false);
    }

    @Test(expected = NodeException.class)
    public void testDeleteObjectFromAnotherServerWithWrongRequest() throws Exception {
        serverNode.setMaster(false);
        ObjectValue barsikCat = new ObjectValue("Barsik", "{}");

        when(httpService.sendDelete(anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respOK);
        objectService.deleteObject(objectCollection.getName(), barsikCat.getName(),  "", false);
    }


    @Test
    public void testUpdateObjectOnMaster() throws Exception {
        when(valueDAO.read(anyString(), anyString())).thenReturn(baronCat);
        objectService.updateObject(objectCollection.getName(), baronCat, "", true);
        assertEquals(objectService.readObject(objectCollection.getName(), baronCat.getName(), "", true), baronCat);

    }
    @Test
    public void testUpdateObjectFromAnotherServer() throws Exception {
        serverNode.setMaster(true);
        Map<String, Object> respGet = new HashMap<>();
        respGet.put("statusCode", 200);

        respGet.put(Constants.RESPONSE_BODY, JsonService.getJSON(baronCat));

        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respGet);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        when(httpService.sendPost(anyString(), anyString(), anyString())).thenReturn(respCreate);
        when(valueDAO.read(anyString(), anyString())).thenReturn(baronCat);
        objectService.createObject(objectCollection.getName(), baronCat,  "", false);
        objectService.updateObject(objectCollection.getName(), baronCat, "", false);
    }

    @Test(expected = FatalErrorException.class)
    public void testUpdateObjectFromAnotherServerWithWrongRequest() throws Exception {
        serverNode.setMaster(false);
        Map<String, Object> respGet = new HashMap<>();
        respGet.put("statusCode", 200);
        respGet.put(Constants.RESPONSE_BODY, JsonService.getJSON(baronCat));

        when(httpService.sendPut(eq("127.0.0.1:8484"), anyString(), anyString())).thenReturn(respBad);

        when(httpService.sendPut(eq("127.0.0.1:8080"), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq("127.0.0.1:8080"), eq("/app/collection/Cats/Baron"), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respGet);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        when(valueDAO.read(anyString(), anyString())).thenReturn(baronCat);
        objectService.updateObject(objectCollection.getName(), baronCat, "", false);
    }

    @Test(expected = NodeException.class)
    public void testUpdateObjectFromAnotherServerWithWrongRequest2() throws Exception {
        serverNode.setMaster(false);
        Map<String, Object> respGet = new HashMap<>();
        respGet.put("statusCode", 200);
        respGet.put(Constants.RESPONSE_BODY, JsonService.getJSON(baronCat));

        when(httpService.sendPut(eq("127.0.0.1:8484"), anyString(), anyString())).thenReturn(respBad);

        when(httpService.sendPut(eq("127.0.0.1:8080"), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq("127.0.0.1:8080"), eq("/app/collection/Cats/Baron"), anyString())).thenReturn(respOK);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respGet);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        when(valueDAO.read(anyString(), anyString())).thenReturn(baronCat);
        objectService.updateObject(objectCollection.getName(), baronCat, "", false);
    }

    @Test(expected = NodeException.class)
    public void testUpdateObjectHealthCheckIsFalse() throws Exception {
        serverNode.setMaster(false);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        serverNode.setHealth(false);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        objectService.updateObject(objectCollection.getName(), baronCat, "", false);
    }

    @Test(expected = NodeException.class)
    public void testUpdateObjectDoNotReadObject() throws Exception {
        serverNode.setMaster(false);
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        objectService.updateObject(objectCollection.getName(), baronCat, "", false);
    }


    @Test
    public void testReadObjectsFromMaster() throws Exception {
        List<ObjectValue> objectValueList = new ArrayList<>();
        objectValueList.add(baronCat);
        objectValueList.add(new ObjectValue("Barsik", "{}"));
        when(valueDAO.read(anyString(), anyString(), anyString())).thenReturn(objectValueList);

        List<ObjectValue> objectValueList2 = objectService.readObjects(objectCollection.getName(), "", true, "10", "");
        assertEquals(objectValueList2, objectValueList);
    }

    @Test
    public void testReadObjectsFromAnotherServer() throws Exception {
        List<ObjectValue> objectValueList = new ArrayList<>();
        ObjectValue barsik = new ObjectValue("Barsik", "{}");
        objectValueList.add(baronCat);
        objectValueList.add(barsik);
        Map<String, Object> respGet = new HashMap<>();
        respGet.put("statusCode", 200);
        respGet.put(Constants.RESPONSE_BODY, JsonService.getJsonArray(objectValueList).toString());
        when(httpService.sendGet(eq(serverNode2.getAddress()), anyString())).thenReturn(respGet);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);

        List<ObjectValue> objectValueList2 = objectService.readObjects(objectCollection.getName(), "", false, "10", "");
        assertEquals(objectValueList2, objectValueList);
    }

    @Test(expected = NodeException.class)
    public void testReadObjectsHealthCheckIsFalse() throws Exception {
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        serverNode2.setHealth(false);
        objectService.readObjects(objectCollection.getName(), "", false, "10", "");
    }


    @Test(expected = NodeException.class)
    public void testReadObjectsFromAnotherServerWithWrongRequest() throws Exception {
        when(httpService.sendGet(anyString(), anyString())).thenReturn(respBad);
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        objectService.readObjects(objectCollection.getName(), "", false, "10", "");
    }

    @Test(expected = NodeException.class)
    public void testReadObjectsExecutionException() throws Exception {
        when(httpService.sendGet(anyString(), eq("/app/info"))).thenReturn(respOK);
        when(httpService.sendGet(anyString(), AdditionalMatchers.not(eq("/app/info")))).thenAnswer(invocationOnMock -> NodeException.class);
        objectService.readObjects(objectCollection.getName(), "", false, "10", "");
    }


}