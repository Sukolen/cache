package by.training.services;

import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.exceptions.MergeConflictException;
import by.training.exceptions.NotFoundException;
import by.training.ifaces.ICache;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Ruslan on 23.01.2018.
 */
public class ObjectServiceTest {

    private Map<String, ICache<String, ObjectValue>> cacheMap;
    private ObjectCollection objectCollection;
    private ObjectValue barsik;
    private IValueDAO valueDAO;
    private ICollectionDAO collectionDAO;
    private ObjectService objectService;

    @Before
    public void testBefore() throws Exception {
        cacheMap = new HashMap<>();
        objectCollection = new ObjectCollection("Cats", "LFU", 10, "{}");
        cacheMap.put(objectCollection.getName(), objectCollection.getType().getCache(objectCollection.getMaxCount()));
        barsik = new ObjectValue("Barsik", "{}");

        valueDAO = mock(IValueDAO.class);
        collectionDAO = mock(ICollectionDAO.class);
        objectService = new ObjectService(cacheMap, collectionDAO, valueDAO);
    }
    @Test
    public void testCreateObject() throws Exception {
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        objectService.createObject(objectCollection.getName(), barsik);
        assertEquals(cacheMap.get(objectCollection.getName()).get(barsik.getName()), barsik);
    }

    @Test
    public void testReadObject() throws Exception {
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        objectService.createObject(objectCollection.getName(), barsik);
        assertEquals(objectService.readObject(objectCollection.getName(), barsik.getName()), barsik);
    }

    @Test(expected = NotFoundException.class)
    public void testReadCollectionIsNotInDB() throws Exception {
        objectService.createObject("Dogs", barsik);
    }

    @Test
    public void testReadNonCacheObject() throws Exception {
        ObjectValue tom = new ObjectValue("Tom", "{}");
        when(valueDAO.read(objectCollection.getName(), "Tom")).thenReturn(tom);
        assertEquals(objectService.readObject(objectCollection.getName(), "Tom"), tom);
    }

    @Test(expected = NotFoundException.class)
    public void testReadNotFoundObject() throws Exception {
        objectService.readObject(objectCollection.getName(), "Tom");
    }

    @Test
    public void testReadNonCacheCollections() throws Exception {
        objectCollection = new ObjectCollection("Dogs", "LFU", 10, "{}");
        when(collectionDAO.read("Dogs")).thenReturn(objectCollection);
        ObjectValue objectValue = new ObjectValue("Bim", "{}");
        objectService.createObject("Dogs", objectValue);
        assertEquals(objectService.readObject(objectCollection.getName(), objectValue.getName()), objectValue);
    }

    @Test
    public void testUpdateObject() throws Exception {
        ObjectValue newBarsik = new ObjectValue("Barsik", "{new}");
        when(valueDAO.read(anyString(), anyString())).thenReturn(barsik);
        objectService.updateObject(objectCollection.getName(), newBarsik);
        assertEquals(cacheMap.get(objectCollection.getName()).get(barsik.getName()).getValue(), newBarsik.getValue());
    }

    @Test
    public void testDeleteObject() throws Exception {
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        objectService.createObject(objectCollection.getName(), barsik);
        objectService.createObject(objectCollection.getName(), new ObjectValue("vasya", "{}"));
        objectService.deleteObject(objectCollection.getName(), barsik.getName());
        objectService.deleteObject("cows", "barsik");
        assertEquals(cacheMap.get(objectCollection.getName()).size(), 1);
        assertEquals(cacheMap.get(objectCollection.getName()).containsKey(barsik.getName()), false);

    }

    @Test
    public void testUpdateOrCreateObject() throws Exception {
        ObjectValue vasya = new ObjectValue("vasya", "{}");
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        objectService.updateOrCreateObject(objectCollection.getName(), vasya);
        assertEquals(cacheMap.get(objectCollection.getName()).get(vasya.getName()), vasya);
    }

    @Test(expected = MergeConflictException.class)
    public void testUpdateOrCreateObjectWithMergeConflict() throws Exception {
        when(collectionDAO.read("Cats")).thenReturn(objectCollection);
        when(valueDAO.read("Cats", "Barsik")).thenReturn(barsik);
        objectService.updateOrCreateObject(objectCollection.getName(), new ObjectValue("Barsik", "{\"name\":\"newName\"}"));
    }




    @After
    public void testAfter() throws Exception {
        barsik = null;
        objectCollection = null;
        cacheMap = null;
        valueDAO = null;
        objectService = null;
    }


}