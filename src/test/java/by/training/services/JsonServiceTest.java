package by.training.services;

import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.services.utils.JsonService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Flash on 07.12.2017.
 */
public class JsonServiceTest {

    @Test
    public void testGetCollectionFromJSON() throws Exception {
        ObjectCollection objectCollection = new ObjectCollection("cats", "LRU", 10, "{}");
        ObjectCollection jsonCollection = JsonService.getCollectionFromJSON("cats", "{'type':'LRU', 'maxCount': 10, 'jsonSchema':{}}");
        assertEquals(objectCollection, jsonCollection);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCollectionFromJSONWithException() throws Exception {
        JsonService.getCollectionFromJSON("cats", "{'maxCount': 10, 'jsonSchema':{}}");
    }

    @Test
    public void testGetCollectionFromJSONWithoutSchema() throws Exception {
        ObjectCollection objectCollection = new ObjectCollection("cats", "LRU", 10);
        ObjectCollection jsonCollection = JsonService.getCollectionFromJSONWithoutSchema("{'name':'cats', 'type':'LRU', 'maxCount': 10, 'jsonSchema':{}}");
        assertEquals(objectCollection, jsonCollection);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCollectionFromJSONWithoutSchemaWithException() throws Exception {
        JsonService.getCollectionFromJSONWithoutSchema("{'name':'cats', 'maxCount': 10, 'jsonSchema':{}}");

    }

    @Test
    public void testGetJSONFromCollections() throws Exception {
        ObjectCollection objectCollection = new ObjectCollection("cats", "LRU", 10, "{}");
        ObjectCollection objectCollection2 = new ObjectCollection("dogs", "LFU", 20, "{}");

        List<ObjectCollection> list = new ArrayList<>();
        list.add(objectCollection);
        list.add(objectCollection2);
        String json = JsonService.getJSONFromCollections(list);

        Gson gson = new Gson();
        JsonArray jsonArray = gson.toJsonTree(list).getAsJsonArray();
        for (JsonElement jsonElement: jsonArray) {
            jsonElement.getAsJsonObject().remove("jsonSchema");
        }
        assertEquals(json, jsonArray.toString());


    }

    @Test
    public void testGetObjectsFromJson() throws Exception {
        ObjectValue objectValue = new ObjectValue("tom", "{}");
        ObjectValue objectValue2 = new ObjectValue("barsik", "{}");

        List<ObjectValue> list = new ArrayList<>();
        list.add(objectValue);
        list.add(objectValue2);
        String json = JsonService.getJSONFromObjects(list);

        List<ObjectValue> objectValues = JsonService.getObjectsFromJson(json);

        assertEquals(list, objectValues);
    }

    @Test
    public void testGetObjectFromJSON() throws Exception {
       ObjectValue objectValue = new ObjectValue("tom", "{}");
       ObjectValue objectValueFromJson = JsonService.getObjectFromJSON("{\"name\":\"tom\",\"value\":\"{}\"}");

       assertEquals(objectValue, objectValueFromJson);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetObjectFromJSONwithException() throws Exception {
        ObjectValue objectValueFromJson = JsonService.getObjectFromJSON("{\"name\":\"tom\",\"val\":\"{}\"}");

    }
    @Test
    public void testGetJSONFromValues() throws Exception {
        ObjectValue objectValue = new ObjectValue("tom", "{}");
        ObjectValue objectValue2 = new ObjectValue("barsik", "{}");

        List<ObjectValue> list = new ArrayList<>();
        list.add(objectValue);
        list.add(objectValue2);
        String json = JsonService.getJSONFromObjects(list);

        Gson gson = new Gson();
        JsonArray jsonArray = gson.toJsonTree(list).getAsJsonArray();
        assertEquals(json, jsonArray.toString());
    }



    @Test
    public void testGetJSONFromCollection() throws Exception {
        ObjectCollection objectCollection = new ObjectCollection("cats", "LRU", 10, "{}");
        String json =  JsonService.getJSON(objectCollection);
        assertEquals(json, "{\"name\":\"cats\",\"type\":\"LRU\",\"maxCount\":10,\"jsonSchema\":\"{}\"}");
    }

    @Test
    public void testValidateJSON() throws Exception {
        String schema = "{\n" +
                "    \"properties\": {\n" +
                "        \"name\": {\n" +
                "            \"type\": \"string\"\n" +
                "        },\n" +
                "        \"age\": {\n" +
                "            \"description\": \"Age in years\",\n" +
                "            \"type\": \"integer\",\n" +
                "            \"minimum\": 0\n" +
                "        }\n" +
                "    },\n" +
                "    \"required\": [\"name\", \"age\"]\n" +
                "}";
        String json = "{\"name\":\"tom\", \"age\":10}";
        assertEquals(true, JsonService.validateJSON(schema, json));


    }
}