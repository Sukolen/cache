package by.training.services;

import by.training.beans.GroupNode;
import by.training.beans.ObjectCollection;
import by.training.beans.ObjectValue;
import by.training.beans.ServerNode;
import by.training.exceptions.NodeException;
import by.training.ifaces.ICollectionDAO;
import by.training.ifaces.IValueDAO;
import by.training.services.utils.HttpService;
import by.training.services.utils.ThreadExecutorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Flash on 27.02.2018.
 */
public class RebalanceServiceTest {
    private RebalanceService rebalanceService;
    private IValueDAO valueDAO;
    private ICollectionDAO collectionDAO;
    private HttpService httpService;
    private ThreadExecutorService executorService;
    private ServerNode serverNode = new ServerNode();
    private ServerNode serverNode2 = new ServerNode();
    private ServerNode serverNode3 = new ServerNode();
    private ServerNode serverNode4 = new ServerNode();
    private Map<String, Object> respOK = new HashMap<>();
    private Map<String, Object> respBad = new HashMap<>();
    private List<ObjectCollection> objectCollections;
    private List<ObjectValue> objectValues;

    @Before
    public void setUp() throws Exception {
        executorService = new ThreadExecutorService(10);
        valueDAO = mock(IValueDAO.class);
        collectionDAO = mock(ICollectionDAO.class);
        httpService = mock(HttpService.class);

        List<ServerNode> servers = new ArrayList<>();
        List<ServerNode> servers2 = new ArrayList<>();
        List<GroupNode> groupServers = new ArrayList<>();
        serverNode.setAddress("127.0.0.1:8080");
        serverNode.setMaster(true);
        serverNode.setHealth(true);
        servers.add(serverNode);
        serverNode4.setAddress("127.0.0.1:8484");
        serverNode4.setHealth(true);
        servers.add(serverNode4);

        GroupNode groupNode = new GroupNode(servers);
        groupNode.setIsMaster(true);
        groupServers.add(groupNode);
        serverNode2.setAddress("127.0.0.1:8181");
        serverNode2.setHealth(true);
        servers2.add(serverNode2);
        serverNode3.setAddress("127.0.0.1:8282");
        serverNode3.setHealth(true);
        servers2.add(serverNode3);

        GroupNode groupNode2 = new GroupNode(servers2);
        groupServers.add(groupNode2);

        respOK.put("statusCode", 200);
        respBad.put("statusCode", 400);

        objectCollections = new ArrayList<>();
        ObjectCollection cats = new ObjectCollection("Cats", "LFU", 10, "{}");
        ObjectCollection dogs = new ObjectCollection("Dogs", "LRU", 20, "{}");
        objectCollections.add(cats);
        objectCollections.add(dogs);

        objectValues = new ArrayList<>();
        objectValues.add(new ObjectValue("Tom", "{}"));
        objectValues.add(new ObjectValue("Barsik", "{}"));
        objectValues.add(new ObjectValue("Tuzik", "{}"));

        rebalanceService = new RebalanceService(collectionDAO, valueDAO, executorService, groupServers, httpService);
    }

    @After
    public void tearDown() throws Exception {
        executorService.getExecutor().shutdownNow();
    }

    @Test
    public void testSetIsRebalanceCollection() throws Exception {
        rebalanceService.setIsRebalanceCollection(true);
        assertEquals(true, rebalanceService.getIsRebalanceCollection());
    }

    @Test
    public void testRebalanceCollection() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        rebalanceService.rebalanceCollection();
    }

    @Test(expected = NodeException.class)
    public void testRebalanceCollectionWithWrongRequest() throws Exception {

        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq("127.0.0.1:8484"), anyString(), anyString())).thenReturn(respBad);
        rebalanceService.rebalanceCollection();
    }

    @Test
    public void testRebalanceCollectionWithWrongRequest2() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq("127.0.0.1:8484"), anyString(), anyString()))
                .thenReturn(respBad)
                .thenReturn(respOK);
        rebalanceService.rebalanceCollection();
    }

    @Test(expected = NodeException.class)
    public void testRebalanceCollectionExecuteException() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenAnswer(invocationOnMock -> NodeException.class);
        rebalanceService.rebalanceCollection();
    }

    @Test
    public void testRebalanceObject() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(valueDAO.read("Cats", "100", "")).thenReturn(objectValues);
        rebalanceService.rebalanceObject();
    }

    @Test(expected = NodeException.class)
    public void testRebalanceObjectWithWrongRequest() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq("127.0.0.1:8282"), anyString(), anyString())).thenReturn(respBad);
        when(valueDAO.read("Cats", "100", "")).thenReturn(objectValues);
        rebalanceService.rebalanceObject();
    }

    @Test
    public void testRebalanceObjectWithWrongRequest2() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenReturn(respOK);
        when(httpService.sendPut(eq("127.0.0.1:8282"), anyString(), anyString())).thenReturn(respBad).thenReturn(respOK);
        when(valueDAO.read("Cats", "100", "")).thenReturn(objectValues);
        rebalanceService.rebalanceObject();
    }

    @Test(expected = NodeException.class)
    public void testRebalanceObjectExecuteException() throws Exception {
        when(collectionDAO.read("100", "")).thenReturn(objectCollections);
        when(valueDAO.read("Cats", "100", "")).thenReturn(objectValues);
        when(httpService.sendPut(anyString(), anyString(), anyString())).thenAnswer(invocationOnMock -> NodeException.class);
        rebalanceService.rebalanceObject();
    }


}